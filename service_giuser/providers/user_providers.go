/*
Package providers
Copyright (c) 2023 Erich Gatejen
[LICENSE]

The user provider.  It is just a simple read only file for the purposes of demonstration.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package providers

import (
	"encoding/json"
	"fmt"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/gservices/service_giuser/local"
	"go.uber.org/zap"
	"net/url"
	"os"
	"strconv"
	"strings"
)

// We could just control the userinfo in the api models, but that can get dangerous and I  have
// some plans around data sourcing.  So I'll split these up.

type UserInfo struct {
	Username  string `json:"username,omitempty"`
	Firstname string `json:"firstname,omitempty"`
	Lastname  string `json:"lastname,omitempty"`
}

type UserFinances struct {
	Username string `json:"username,omitempty"`
	Coins    int    `json:"coins,omitempty"`
}

type UserProvider interface {
	GetUserInfo(username string) (UserInfo, error)
	GetUserFinances(username string) (UserFinances, error)
	ExpendUserCoins(username string, number int) (int, string, error)
}

func GetUserProvider(gcontext *local.GContext, uri string) (UserProvider, error) {
	var (
		provider UserProvider
		err      error
	)

	parsedURL, err := url.Parse(uri)
	if err != nil {
		panic(fmt.Sprintf("Poorly formed user provider uri: %s : %v", uri, err))
	}

	switch strings.ToLower(parsedURL.Scheme) {
	case "file":
		provider, err = GetUserProviderFile(gcontext, parsedURL.Host+parsedURL.Path)
		break

	default:
		err = base.NewGinfraError(fmt.Sprintf(
			"ERROR: Unknown UserProvider type: %s", strings.ToLower(parsedURL.Scheme)))
	}

	return provider, err
}

// = FILE PROVIDER ====================================================================================================
// Mostly for testing.

type userProviderFile struct {
	users map[string]interface{}
}

func loadGetUserProviderFile(location string) (UserProvider, error) {
	local.Logger.Info("Attempting to load User Provider.", zap.String("location", location))

	var result userProviderFile
	data, err := os.ReadFile(location)
	if err == nil {
		err = json.Unmarshal(data, &result.users)
	}
	return &result, err
}

func GetUserProviderFile(gcontext *local.GContext, location string) (UserProvider, error) {
	// See if file exists in ExecutableDirPath and if still not, just the raw path.
	result, err := loadGetUserProviderFile(gcontext.Scfg.ServiceHome + "/" + location)
	if err != nil {
		result, err = loadGetUserProviderFile(location)
	}
	return result, err
}

func getField(source interface{}, field string) (string, error) {
	src, ok := source.(map[string]interface{})
	if !ok {
		return "", base.NewGinfraErrorFlags("User provider file data is fundamentally corrupted.",
			base.ERRFLAG_SUBSYSTEM_SERIOUS)
	}
	s, ok := src[field].(string)
	if !ok {
		return "", base.NewGinfraErrorAFlags("User provider file data is bad.", base.ERRFLAG_SUBSYSTEM_SERIOUS,
			base.LM_CAUSE, field)
	}
	return s, nil
}

func (prov *userProviderFile) GetUserInfo(username string) (UserInfo, error) {
	var (
		err error = nil
		r   UserInfo
	)

	i, ok := prov.users[username]
	if !ok {
		err = base.NewGinfraErrorFlags("User not found", base.ERRFLAG_NOT_FOUND)
	} else {
		r.Username, err = getField(i, "username")
		if err == nil {
			r.Firstname, err = getField(i, "firstname")
			if err == nil {
				r.Lastname, err = getField(i, "lastname")
			}
		}
	}

	return r, err
}

func (prov *userProviderFile) GetUserFinances(username string) (UserFinances, error) {
	var (
		err error
		r   UserFinances
		c   string
	)

	i, ok := prov.users[username]
	if !ok {
		err = base.NewGinfraError("User not found")
	} else {
		if c, err = getField(i, "coins"); err == nil {
			r.Username = username
			if r.Coins, err = strconv.Atoi(c); err != nil {
				err = base.NewGinfraErrorChild("Coins value corrupted.", err)
			}
		}
	}

	return r, err
}

func (prov *userProviderFile) ExpendUserCoins(username string, number int) (int, string, error) {
	var (
		err   error
		cs, h string
		c     int
	)

	i, ok := prov.users[username]
	if !ok {
		err = base.NewGinfraError("User not found")
	} else {
		if cs, err = getField(i, "coins"); err == nil {
			if c, err = strconv.Atoi(cs); err == nil {
				if c < 1 {
					err = base.NewGinfraError("User has no coins.")

				} else if c < number {
					err = base.NewGinfraError("User does not have enough coins.")

				} else {
					c = c - number

					// Transaction is not complete until this succeeds.
					i.(map[string]interface{})["coins"] = strconv.Itoa(c)

					// This is fake for now.
					h = base.GenerateCodeword(20)
				}
			}
		}
	}

	return c, h, err
}
