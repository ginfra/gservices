/*
Package providers
Copyright (c) 2023 Erich Gatejen
[LICENSE]

The user provider.  It is just a simple read only file for the purposes of demonstration.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package providers

import (
	"gitlab.com/ginfra/gservices/service_giuser/local"

	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################

	"gitlab.com/ginfra/ginfra/base"
	// >A###############################################################################################################
	// #  YOUR IMPORTS END
)

type ProvidersLoaded struct {

	// # ADD POINTERS (without * because they are interfaces) TO YOUR PROVIDERS START HERE
	// >B###############################################################################################################

	UserProvider UserProvider

	// >B###############################################################################################################
	// # ADD POINTERS (without * because they are interfaces) TO YOUR PROVIDERS END HERE

}

func ProvidersLoad(gcontext *local.GContext) (*ProvidersLoaded, error) {
	var (
		l   ProvidersLoaded
		err error
	)

	// # ADD INSTANTIATING AND REMEMBERING YOUR PROVIDERS START HERE
	// >C###############################################################################################################

	scfg := local.GetConfiguredServiceSpecifics(gcontext)
	if scfg.Err != nil {
		return nil, scfg.Err
	}

	var puri string
	if puri, err = scfg.GetValue(local.ConfigUserProvider).GetValueString(); err == nil {
		l.UserProvider, err = GetUserProvider(gcontext, puri)
	} else {
		err = base.NewGinfraErrorChild("Could not get UserProvider uri configuration item.", err)
	}

	// >C###############################################################################################################
	// # ADD INSTANTIATING AND REMEMBERING YOUR PROVIDERS END HERE

	return &l, err
}
