#!/usr/bin/env bash

##########################################################################
# Place custom entry tasks here to be done before starting the server.
# This file will not be updated, so any additions or changes are safe.

echo "No custom entrypoint." > /service/log/start.log


