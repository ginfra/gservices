//go:build functional_test

package service

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ginfra/gservices/service_giuser/api/service/client"
	"net/http"
	"os"
	"testing"
)

func testUser(t *testing.T, c *client.ClientWithResponses, u, f, l string) {

	if response, err := c.GetGiuserV1UserUsernameWithResponse(context.Background(), u); err == nil {

		assert.Equal(t, http.StatusOK, response.StatusCode(), "Code")
		if response.StatusCode() == http.StatusOK {
			assert.Equal(t, u, *response.JSON200.Username, "Username")
			assert.Equal(t, f, *response.JSON200.Firstname, "Firstname")
			assert.Equal(t, l, *response.JSON200.Lastname, "Lastname")
		}

	} else {
		t.Error(fmt.Errorf("Bad response.  %v", err))
	}
}

// TestServiceGIUser entry point for all tests.
func TestServiceGIUser(t *testing.T) {
	_, _ = setupTest(t, yourSetup)
	// defer teardown(t, tcontext, yourTeardown)  -- no teardown as we are explicitly shutting down the service.

	// == PUT YOUR TESTS HERE  ==================================================================================

	// V1 ================================================================================

	hostname, _ := os.Hostname()
	lc, err := client.NewClientWithResponses("http://" + hostname + ":8900")
	if err != nil {
		t.Error(fmt.Errorf("Could not get client.  %v", err))
		return
	}

	testUser(t, lc, "bob", "Bob", "Bobbersons")
	testUser(t, lc, "jack", "Jack", "Puppersons")
	if response, err := lc.GetGiuserV1UserUsernameWithResponse(context.Background(), "goats"); err == nil {
		assert.Equal(t, http.StatusNotFound, response.StatusCode(), "Code")
	} else {
		t.Error(err)
	}

	// V2 =============================================================================================

	var lc2 *client.ClientWithResponses
	lc2, err = client.NewClientWithResponses("http://" + hostname + ":8900")
	if err != nil {
		t.Error(fmt.Errorf("Could not get client.  %v", err))
		return
	}

	testUserV2(t, lc2, "bob", "Bob", "Bobbersons")
	testUserV2(t, lc2, "jack", "Jack", "Puppersons")

	testCoinUseV2(t, lc2, true, "bob", 10, 90)
	testCoinUseV2(t, lc2, true, "bob", 10, 80)
	testCoinUseV2(t, lc2, false, "bob", 100, 90)
	testCoinUseV2(t, lc2, true, "jack", 50, 50)
	testCoinUseV2(t, lc2, true, "jack", 50, 0)

	// Control  =============================================================================================
	testServiceControl(t)
}
