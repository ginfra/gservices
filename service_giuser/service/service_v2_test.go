//go:build functional_test

package service

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ginfra/gservices/service_giuser/api/service/client"
	"net/http"
	"testing"
	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################
	// >A###############################################################################################################
	// #  YOUR IMPORTS END
)

func testUserV2(t *testing.T, c *client.ClientWithResponses, u, f, l string) {

	if response, err := c.GetGiuserV2UserUsernameWithResponse(context.Background(), u); err == nil {
		assert.Equal(t, http.StatusOK, response.StatusCode(), "Code")
		if response.StatusCode() == http.StatusOK {
			assert.Equal(t, u, *response.JSON200.Username, "Username")
			assert.Equal(t, f, *response.JSON200.Firstname, "Firstname")
			assert.Equal(t, l, *response.JSON200.Lastname, "Lastname")
		}

	} else {
		t.Error(err)
	}
}

func testCoinUseV2(t *testing.T, c *client.ClientWithResponses, succeed bool, u string, coins, remain int) {

	response, err := c.GetGiuserV2UserUsernameCoinsUseNumbercoinsWithResponse(context.Background(), u, coins)
	if err == nil {
		if succeed {
			assert.Equal(t, http.StatusOK, response.StatusCode(), "Code")
			if response.StatusCode() == http.StatusOK {
				assert.Equal(t, remain, *response.JSON200.CoinsRemaining, "Coins remaining")
			}
		} else {
			assert.NotEqual(t, http.StatusOK, response.StatusCode(), "Code")
		}
	}

	if err != nil {
		t.Error(err)
	}
}
