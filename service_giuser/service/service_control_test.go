//go:build functional_test

package service

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ginfra/gservices/service_giuser/api/service/client"
	"gitlab.com/ginfra/gservices/service_giuser/api/service/models"
	"gitlab.com/ginfra/gservices/service_giuser/local"
	"net/http"
	"os"
	"testing"
	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################
	// >A###############################################################################################################
	// #  YOUR IMPORTS END
)

/*
	IMPORTANT: Add the following two lines to the start of any Test* function.
		teardown, tcontext := setupTest(t, yourSetup)
		defer teardown(t, tcontext, yourTeardown)
*/
// ^^###################################################################################################################
// # ADDITIONAL CODE GOES BELOW HERE

// testServiceGIUser entry point control tests
func testServiceControl(t *testing.T) {
	// _, _ = setupTest(t, yourSetup)
	// teardown(t, tcontext, yourTeardown)  No teardown needed.  We are explicitly stopping it.

	// == PUT YOUR TESTS HERE  ==================================================================================

	hostname, _ := os.Hostname()
	lc, err := client.NewClientWithResponses("http://" + hostname + ":8900")
	if err != nil {
		t.Error(fmt.Errorf("Could not get client.  %v", err))
		return
	}

	var p models.GiControlManageStopParams
	a := local.TestAuth
	p.Auth = &a

	if response, err := lc.GiControlManageStopWithResponse(context.Background(), &p); err == nil {
		assert.Equal(t, http.StatusOK, response.StatusCode(), "Code")
	} else {
		t.Error(err)
	}

}
