/*
Package service
Copyright (c) 2023 Erich Gatejen
[LICENSE]

# Service implementation for V1

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package service

import (
	"context"
	"github.com/labstack/echo/v4"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common/service/gotel"
	"gitlab.com/ginfra/gservices/service_giuser/api/service/models"
	"gitlab.com/ginfra/gservices/service_giuser/local"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"net/http"

	"go.uber.org/zap"
)

func (gis *Servicegiuser) GetGiuserV1UserUsername(ctx echo.Context, username models.Giusername) error {
	var (
		span     trace.Span
		userinfo *models.Userinfov1
	)

	o := gotel.GetOtelConfig()
	if o != nil {
		_, span = o.Tracer.Start(context.Background(), "GetGiuserV1UserUsername")
		defer span.End()
		span.SetAttributes(attribute.Key("username").String(username))
	}

	user, err := gis.Providers.UserProvider.GetUserInfo(username)
	if err == nil {
		userinfo = &models.Userinfov1{
			Firstname: &user.Firstname,
			Lastname:  &user.Lastname,
			Username:  &user.Username,
		}
		err = ctx.JSON(http.StatusOK, userinfo)

	} else {
		switch err.(type) {
		case *base.GinfraError:
			gerr := err.(*base.GinfraError)
			if gerr.GetFlags()&base.ERRFLAG_NOT_FOUND > 0 {
				err = ctx.JSON(http.StatusNotFound, "User not found.")
			} else {
				local.Logger.Error("Internal error while getting user.", zap.String(base.LM_CAUSE, err.Error()))
				err = ctx.JSON(http.StatusInternalServerError, "Internal error.")
			}

		case error:
			local.Logger.Error("Spurious error breaks call.", zap.String(base.LM_CAUSE, err.Error()))
			err = ctx.JSON(http.StatusInternalServerError, "Unknown error.")

		default:
			panic(err)
		}

	}

	if err != nil && o != nil {
		span.AddEvent("error", trace.WithAttributes(
			attribute.String("error.message", err.Error()),
		))
	}

	return err
}
