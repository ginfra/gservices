/*
Package service
Copyright (c) 2023 Erich Gatejen
[LICENSE]

# Service implementation for V1

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package service

import (
	"context"
	"github.com/labstack/echo/v4"
	"gitlab.com/ginfra/ginfra/common/service/gotel"
	"gitlab.com/ginfra/gservices/service_giuser/api/service/models"
	"gitlab.com/ginfra/gservices/service_giuser/local"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"net/http"
	"strconv"

	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################
	// >A###############################################################################################################
	// #  YOUR IMPORTS END
	"gitlab.com/ginfra/gservices/service_giuser/providers"
)

func (gis *Servicegiuser) GetGiuserV2UserUsername(ctx echo.Context, username models.Giusername) error {

	var (
		userinfo *models.Userinfov2
		uf       providers.UserFinances
		span     trace.Span
	)

	o := gotel.GetOtelConfig()
	if o != nil {
		_, span = o.Tracer.Start(context.Background(), "GetGiuserV2UserUsername")
		defer span.End()
		span.SetAttributes(attribute.Key("username").String(username))
	}

	user, err := gis.Providers.UserProvider.GetUserInfo(username)
	if err == nil {
		uf, err = gis.Providers.UserProvider.GetUserFinances(username)
	}
	if err == nil {
		local.Logger.Info("Got information for user.", zap.String("username", user.Username),
			zap.String("first.name", user.Firstname), zap.String("last.name", user.Lastname),
			zap.String("coins", strconv.Itoa(uf.Coins)))
	}

	if err == nil {
		userinfo = &models.Userinfov2{
			Firstname: &user.Firstname,
			Lastname:  &user.Lastname,
			Username:  &user.Username,
			Coins:     &uf.Coins,
		}
		err = ctx.JSON(http.StatusOK, userinfo)

	} else {
		err = ctx.JSON(http.StatusNotFound, "")
	}

	if err != nil && o != nil {
		span.AddEvent("error", trace.WithAttributes(
			attribute.String("error.message", err.Error()),
		))
	}

	return err
}

func (gis *Servicegiuser) GetGiuserV2UserUsernameCoinsUseNumbercoins(ctx echo.Context, username models.Giusername,
	numbercoins int) error {

	var span trace.Span

	o := gotel.GetOtelConfig()
	if o != nil {
		_, span = o.Tracer.Start(context.Background(), "GetGiuserV2UserUsernameCoinsUseNumbercoins")
		defer span.End()
		span.SetAttributes(attribute.Key("username").String(username))
		span.SetAttributes(attribute.Key("coins").Int(numbercoins))
	}

	var (
		cr *models.Coinreceipt
	)

	rm, hash, err := gis.Providers.UserProvider.ExpendUserCoins(username, numbercoins)

	if err == nil {
		cr = &models.Coinreceipt{
			CoinsRemaining: &rm,
			ReceiptHash:    &hash,
		}
		err = ctx.JSON(http.StatusOK, cr)

	} else {
		err = local.PostErrorResponse(ctx, http.StatusNotAcceptable, err)

	}

	if err != nil && o != nil {
		span.AddEvent("error", trace.WithAttributes(
			attribute.String("error.message", err.Error()),
		))
	}

	return err
}
