# Grand demonstration for hosted

## Prerequisites

This all assumes you have installed the [Ginfra application](https://gitlab.com/ginfra/ginfra).
Please follow the instructions for that before attempting any of this.

The docker build relies on Node.js, so you must [install it](https://nodejs.org/en).  For ubuntu, I
successfully installed it with [these instructions](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm).

The host machine needs to have a running Docker engine (the [docker app](https://www.docker.com/products/docker-desktop/) should be fine).

Some of the services need java to build.  [See our note](https://gitlab.com/ginfra/ginfra/-/blob/main/lang/java/README.md) about Ginfra java and follow any instructions.

You will need to build the containers for the services.  You can do this
by cd'ing into the gservices/integration/scenerios/newdemo/local directory and run the following command:

```task docker```

# Run the demo.

This should work on either various unix systems and modern Windows.  If you have
installed (or build and installed) the ginfra application, then simply run this command:

```ginfra run demo.yaml```

And then point a web browser at http://localhost:8080/.   Both 'bob' and 'jack' are valid
usernames.

You can get to uptrace application at http://localhost:14318/.

## Stopping the demo and cleaning up.

Run the following command:

```ginfra manage host destroy```
