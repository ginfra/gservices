# New Demonstration

These will create a more complicated demonstration stack for local and host deployments.  It is a simple
end-to-end solution.  It has the following:

- Configuration service written in Go.
- User information service written in Go using [Echo](https://echo.labstack.com/).
- A Pet information service written in Java using [Micronaut](https://micronaut.io/). 
- A web application that interacts with the other services using [Vue.js](https://vuejs.org/) over [Vite](https://vitejs.dev/).

All of the services use OpenApi to communicate.  All services but the configuration service have
templates that can build an app for you using the ['ginfra generate'](https://gitlab.com/ginfra/ginfra/-/blob/main/doc/GENERATION.md) 
command.

## Local[README.md](README.md)

This runs all the services as a process on your computer.  It has been tested on a mid-grade
windows [(why windows?)](https://gitlab.com/ginfra/ginfra/-/blob/main/doc/WHYWINDOWS.md) laptop as well as a debian desktop, so you should have no problems running it.

To run it cd into the gservices/integration/scenerios/newdemo/local directory and read the [README](local/README.md).

## Hosted

There isn't one and there won't be.  It doesn't show anything new.
