# Demonstration for local machine

## Prerequisites

This all assumes you have installed the [Ginfra application](https://gitlab.com/ginfra/ginfra).
Please follow the instructions for that before attempting any of this.

The web service relies on Node.js, so you must [install it](https://nodejs.org/en).  For ubuntu, I
successfully installed it with [these instructions](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm).

You will need to build the services.  You can do this
by cd'ing into the gservices/integration/demo/local directory and run the following command:
```task build```

# Run the demo.

This should work on either various unix systems and modern Windows.  If you have
installed (or build and installed) the ginfra application, then simply run this command:

```ginfra run demo.yaml```

And then point a web browser at http://localhost:8080/.   Both 'bob' and 'jack' are valid 
usernames.

# End to end test.

You can run an end to end test with the following command:

```ginfra run test.yaml```

However, this relies on Firefox and the appropriate Webdriver driver to be installed.  The test result
will be available at gservices/web_userview/tests_output/nightwatch-html-report/index.html.  

If you get an error like "Your Firefox profile cannot be loaded," you don't have the right browser installed.  
As of this writing, the firefox that is installed on Ubuntu by default will cause this error.  You need
to uninstall it and install firefox using apt (rather than their App Center).
