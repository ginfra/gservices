# Demonstration for kubernetes hosted containers

## Prerequisites

This only supports linux based systems at this time.

This demo is for self-hosted kubernetes cluster.  These are [the instructions](https://gitlab.com/ginfra/gplatform/-/tree/main/platform/nix_root/kube?ref_type=heads) I use to build
a self-hosted cluster.   This is for testing purposes only; DO NOT use for production.  Some of the demo commands rely on
configuration done by the above instructions, particularly around ingress, which uses Metallb and nginx-ingress through
port 80.

This all assumes you have installed the [Ginfra application].  The above installation will also install ginfra on the node1cp
control plane node.  If you don't use the installation, please follow [the instructions](https://gitlab.com/ginfra/ginfra)
for that before attempting any of this.  Either way, you will need to build the application, so go to wherever the ginfra
repo is (/build/ginfra if you used the installation above) and run the following command.

```
task install
```

Note that the above instructions will clone the ginfra/gservices repo (where you see this README.md) into node1cp into
the directory /build/gservices.   It is most convenient to run the demo there, since it will have kubectl installed
and working.

The containers are already built and are in the docker.io repo, and they are the latest tested versions.

## Configuring the demo.

Run the following no the control plane node (named node1cp if you used the above install).

```
kubectl get svc -A
```

Find the ingress LoadBalancer in the output.  It will look like this.

```
ingress-nginx      ingress-nginx-controller             LoadBalancer   10.101.131.210   192.168.1.230   80:32393/TCP,443:32312/TCP   41h
```

The IP you want in this example is 192.168.1.230.  This IP was part of a range configured by [this file] (https://gitlab.com/ginfra/gplatform/-/blob/main/platform/nix_root/kube/assets/lb_config.yaml.TPL?ref_type=heads)
(note that the file will be called ../lb_config.yaml during the install process).  If you altered that file for a different
IP range, you will see a different IP in the above command.

Edit /etc/hosts on any machine involved in the demo, which if you used the above install will be 'node1cp' and 'node2wk'.
If you use a different machine to run the Ginfra app, you will need to edit /etc/hosts there too.  Add the following line:

```
192.168.1.230   demo.localdomain
```

On whatever system you plan to use the browser to go to the web page, you'll need to add it to the hosts file there too.  
(Yes, windows has one too.)

Using whatever IP you got from the command above.

Finally, we need to add the ingress.  From the same directory as this README.md, run the following:

```
kubectl apply -f ingress.yaml
```

If you used a different ingress host name that demo.ginfra.tech, you will need to edit ingress.yaml to change the host.

## Running the demo.

Then simply run this command:

```ginfra run demo.yaml```

## Stopping the demo and cleaning up.

Run the following command: 

```ginfra kube destroy```

## End to end test.

To run the demo with tests, run this command:

```ginfra run test.yaml```

One of the containers will use webdriver to hit the GUI.

