# Demonstration

These will create the demonstration stack for local and host deployments.  It is a simple
end-to-end solution.  It has a configuration provider service, a simple user information
service and a Vue based web application that can fetch user information.  It has both local and
hosted (docker container) versions.

## Prerequisites

This all assumes you have installed the [Ginfra application](https://gitlab.com/ginfra/ginfra).
Please follow the instructions for that before attempting any of this.  

The web service replies on Node.js, so you must [install it](https://nodejs.org/en).  Note the
[hosted version](#Hosted) has what it needs cooked into the container, so you will not need to 
install it there.

https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

Additionally, the hosted version uses [Docker Engine](https://www.docker.com/) which must be installed 
and running.  Docker Desktop should work as well.

## Local

This runs all the services as a process on your computer.  It has been tested on a mid-grade
windows [(why windows?)](https://gitlab.com/ginfra/ginfra/-/blob/main/doc/WHYWINDOWS.md) laptop as well as a debian desktop, so you should have no problems running it.

To run it cd into the gservices/integration/demo/local directory and read the [README](local/README.md).

## Hosted

This runs all the services in docker containers.  For now, it assumes the docker engine is running
on the same system as where start it.  

To run it cd into the gservices/integration/demo/hosted directory and read the [README](host/README.md).




