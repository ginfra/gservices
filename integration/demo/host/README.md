# Demonstration for hosted containers

## Prerequisites

This all assumes you have installed the [Ginfra application](https://gitlab.com/ginfra/ginfra).
Please follow the instructions for that before attempting any of this.

The web service relies on Node.js, so you must [install it](https://nodejs.org/en).  For ubuntu, I 
successfully installed it with [these instructions](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm).

Additionally, the hosted version uses [Docker Engine](https://www.docker.com/) which must be installed
and running.  Docker Desktop should work as well.

You will need to build the containers.  You can do this
by cd'ing into the gservices/integration/demo/host directory and run the following command:
```task build```

## Running the demo.

This should work on either various unix systems and modern Windows.  You must have a working
docker host running.  You will also need the ginfra application built and installed.

Then simply run this command:

```ginfra run demo.yaml```

And then point a web browser at http://localhost:8080/.   Both 'bob' and 'jack' are valid 
usernames.

## Running the demo and allow other machines to connect to it.

You can expose the web service to other machines.  To do this, edit the demo_host.yaml
file and change '--host 192.168.1.220' to have the extern IP of the host machine.  Then 
run this command:

```ginfra run demo_host.yaml```

## Stopping the demo and cleaning up.

Run the following command: 

```ginfra manage host destroy```

## End to end test.

To run the demo with tests, run this command:

```ginfra run test.yaml```

One of the containers will use webdriver to hit the GUI.

