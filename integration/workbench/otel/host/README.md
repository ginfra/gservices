# OPEN TELEMETRY WORKBENCH

## Prerequisites

This all assumes you have installed the [Ginfra application](https://gitlab.com/ginfra/ginfra).
Please follow the instructions for that before attempting any of this.

The docker build relies on Node.js, so you must [install it](https://nodejs.org/en).  For ubuntu, I
successfully installed it with [these instructions](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm).

The host machine needs to have a running Docker engine (the [docker app](https://www.docker.com/products/docker-desktop/) should be fine).

If this is the first time run, in the repo root, run the following: task buildgranddemo

### Run the demo

A complete end to end example for open telemetry traces and metrics
making it into uptrace.  You can start it with the command:

```ginfra run demo.yaml```

Run the following to generate data (be sure to fix the IP to your host):

```
    ginfra tool selenium run localhost:8904 http://192.168.1.1:8080 e2e.snp 
```

This assumes you are on the same machine as the docker host.  After a little
while point a browser at [http://localhost:14318/](http://localhost:14318/).
The page will tell you how to login to uptrace, so you can poke around.

NOTE: the servers are run on UTC, so you might have to change the time range in
uptrace to actually see any data.

When you are done, don't forget to shut down the services.

````
    ginfra manage host destroy
```

