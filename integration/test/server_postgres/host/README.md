# Demo

### Running the demo.

This should work on either various unix systems and modern Windows.  You must have a working
docker host running.  You will also need the ginfra application built and installed.

Then simply run this command:

```ginfra run demo.yaml```

It doesn't really do much.   The server_postgres unit tests are sufficient to verify
the database is loaded and can handle the server container features.

### Stoping the demo and cleaning up.

Run the following command:

```ginfra manage host destroy```



