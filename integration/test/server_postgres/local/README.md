# Demonstration for local machine

This should work on either various unix systems and modern Windows.  You should have
build and installed the ginfra application.

You will need a working postgres server.  The container provided by gservices/server_postgres works 
just fine.   You can build the container by running 'task docker' in that directory.  You can then
start the container with following command:

```docker run -d -p 5432:5432 gservices/server_postgres NOSERVICE```

You can get the postgres user password by running the following:

docker exec -it CONTAINER_ID cat /postgres_password

You can get the CONTAINER_ID by running 'docker ps' and finding it in the list.  Make a copy
of the file postgres.config.SOURCE and name it postgres.config.  Edit it.  Change
CHANGEME_PASSWORD to the password you got in the previous command.  Change CHANGEME_HOST
to the host name or ip of the host for the container.  Unless you did some customizations,
it will be the same system where you did the commands above.

You can now run the demo with the following command:

```ginfra run demo.yaml```

It doesn't do much else.

# End to end test.

There are no tests here at this time.  The server_postgres functional tests cover it.  If you want to run the
end to end tests against this instance, change the file 'postgres_url' in the gservices/server_postgres/
directory to have the url from file postgres.config you created above.  And then run the following
from gservices/server_postgres/

```task functional_test```

