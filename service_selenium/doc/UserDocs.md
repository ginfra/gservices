### This file will not be updated, so any additions or changes are safe.

The service can run snippets of java code to drive selenium.  The following is
an example of such a snippet.

```
    driver.findElement(By.cssSelector(".inputusername")).click();
    driver.findElement(By.cssSelector(".inputusername")).sendKeys("bob"); 
    for (int i = 0; i < 1000; i++) {
        driver.findElement(By.cssSelector(".doit")).click();
    }
```

The service will take care of getting a working 'driver' and cleaning up after 
your snippet is done.  The snippet does not have to exit cleanly.  The service 
can interrupt it and stop it.

See the 'ginfra tool selenium' command on how to interact with this service.

