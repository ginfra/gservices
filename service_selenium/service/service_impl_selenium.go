/*
Package service
Copyright (c) 2024 Erich Gatejen
[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt, if unaltered.  You are free to alter and apply any license
you wish to this file.
*/
package service

import (
	"bytes"
	"errors"
	"github.com/labstack/echo/v4"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common"
	"gitlab.com/ginfra/gservices/service_selenium/api/service/models"
	"gitlab.com/ginfra/gservices/service_selenium/local"
	"go.uber.org/zap"
	"hash/crc32"
	"io"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"sync"
	"syscall"
	"time"
)

const CompileCachePath = "store/cache"
const JavaDepsPath = "deps"
const ExecFastFailDelay = 150

const StatusIdle = "IDLE"
const StatusRunning = "RUNNING"
const StatusError = "ERROR"
const StatusDone = "DONE"

var (
	RunCache   = make(map[string]RunCacheItem)
	RunLock    sync.Mutex
	crc32Table = crc32.MakeTable(crc32.IEEE)
	Current    string
	Status     string
	PendingErr error

	cmd *exec.Cmd
)

// === RUN ===========================================================================================

// Keep it simple for now.
var srccode = `import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import sun.misc.Signal;
import sun.misc.SignalHandler;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.io.FileWriter;
import java.net.URI;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.Vector;

class <<<classname>>> {

    static private WebDriver driver = null;
    static private boolean stopped = false;
    private String path;

    static private int assertions = 0;
    static private int assertionsFail = 0;
    static private Vector<String> assertionsFailText = new Vector<String>();

    public static void main(String[] args) {
        <<<classname>>> ping = new <<<classname>>>();
        if (args.length < 3) {
            System.exit(1);
        }

        Signal.handle(new Signal("TERM"), new SignalHandler() {
            public void handle(Signal sig) {
                stopped = true;
                driver = null;
            }
        });

        int i = ping.ping(args[0], args[1], args[2]);

        try {
            BufferedWriter bow = new BufferedWriter(new FileWriter(args[2] + ".json"));
            bow.write("{\n     \"assertions\": " + String.valueOf(assertions) + ",\n");
            bow.write("     \"assertions_fail\": " + String.valueOf(assertionsFail) + ",\n");
            bow.write("     \"assertion_fail_text\": [\n");
            for (int idx = 0; i < assertionsFailText.size(); i++) {
                bow.write("\"" + assertionsFailText.get(idx) + "\"");
                if (idx != assertionsFailText.size() - 1) {
                    bow.write(",");
                }
                bow.write("\n");
            }
            bow.write("     ]\n}");
            bow.close();

        } catch (Exception e) {
            System.out.println("------------------------------------------------------");
            System.out.println("Could not write assert file.");
            System.out.println(e.getMessage());
        }

        System.exit(i);
    }

    private void assertIs(String left, String right) {
        if (Objects.equals(left, right)) {
            assertions++;
        } else {
            assertions++;
            assertionsFail++;
            assertionsFailText.add(left + ":" + right);
        }
    }

    private void Screenshot() {
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        Path src = Paths.get(screenshotFile.getAbsolutePath());
        Path dest = Paths.get(path + ".png");
        try {
            Files.copy(src, dest, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public int ping(String remoteHost, String target, String p) {

		path = p;

        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("headless");
        options.setCapability(CapabilityType.BROWSER_NAME,"firefox");

        try {
            driver = new RemoteWebDriver(URI.create("http://" + remoteHost + "/wd/hub").toURL(), options);

		<<<code>>>

        } catch (Exception e) {
            if (!stopped) {
                System.out.println(e.getMessage());
                return 1;
            }
        } finally {
            if (driver != null) {
                driver.quit();
            }
        }

        return 0;
    }
}
`

type RunCacheItem struct {
	Path string // Will not have the extension suffix, as it applies to both .java and .class.
	Size int64
	Name string
}

func runSetup(gis *Serviceservice_selenium) error {
	Status = StatusIdle
	return os.MkdirAll(filepath.Join(gis.ServiceContext.Scfg.ServiceHome, CompileCachePath), os.ModePerm)
}

func getClassPath(gis *Serviceservice_selenium) string {
	return ".:" + filepath.Join(gis.ServiceContext.Scfg.ServiceHome, JavaDepsPath) + "/*"
}

// processCache calculates the hash of the given block, checks if it exists in the cache,
// compiles the block if necessary, and updates the cache.  The cache contains both the source and binary
// or the class.  Returns the hash as an id, path to the binary and an error, if applicable.
func processCache(gis *Serviceservice_selenium, block string) (string, string, error) {
	h := strconv.Itoa(int(crc32.Checksum([]byte(block), crc32Table)))
	if h == "" {
		return "", "", base.NewGinfraError("Serious write issue in cache.  Could not get crc32")
	}
	name := "H" + h

	// Paths into the cache
	cdir := filepath.Join(gis.ServiceContext.Scfg.ServiceHome, CompileCachePath)
	p := filepath.Join(cdir, name)
	pJava := p + ".java"
	pClass := p + ".class"

	// Cache check.  Must have both the correct source and compiled binary.
	i, ok := RunCache[name]
	if ok {
		ii, err := os.Stat(pJava)
		if err == nil && i.Size == ii.Size() && i.Name == name {
			// Make sure it is compiled.
			if _, err = os.Stat(pClass); err == nil {
				return name, pClass, nil

			} else if !errors.Is(err, os.ErrNotExist) {
				return "", "", base.NewGinfraErrorChildA("Serious read issue in cache.", err)

			}
		}
	}

	// We only do one type of file for now.
	src, err := common.SimpleReplacer(&srccode, map[string]interface{}{"classname": name, "code": block}, false)
	if src == nil || err != nil {
		panic("BUG.  Replacement failed.")
	}

	// Write the source code to the cache file
	err = os.WriteFile(pJava, []byte(*src), 0644)
	if err != nil {
		return "", "", base.NewGinfraErrorChildA("Could not cache code.", err, base.LM_ID, name)
	}

	cp := getClassPath(gis)

	// Compile it.
	cmd := exec.Command("javac", "-classpath", cp, pJava)
	cmd.Dir = cdir
	var sout bytes.Buffer
	cmd.Stdout = &sout
	var serr bytes.Buffer
	cmd.Stderr = &serr

	err = cmd.Run()
	if err != nil {
		return "", "", base.NewGinfraErrorChildA("Compile failed.", err, base.LM_STDOUT,
			sout.String(), base.LM_STDERR, serr.String())
	}

	// Check for executable.
	if _, err = os.Stat(pClass); err != nil {
		return "", "", base.NewGinfraErrorChildA("Compile failed.  No executable.", err, base.LM_STDOUT,
			sout.String(), base.LM_STDERR, serr.String())
	}

	// Update the cache item
	RunCache[name] = RunCacheItem{
		Path: p,
		Size: int64(len(*src)),
		Name: name,
	}

	return name, pClass, nil
}

func runExecutable(gis *Serviceservice_selenium, id, target string) error {

	cp := getClassPath(gis)
	cmd = exec.Command("/run_java.sh", id, cp, id, "localhost:4444", target)
	var sout bytes.Buffer
	cmd.Stdout = &sout
	var serr bytes.Buffer
	cmd.Stderr = &serr
	cmd.Dir = filepath.Join(gis.ServiceContext.Scfg.ServiceHome, CompileCachePath)

	Status = StatusRunning
	go func(c *exec.Cmd) {
		local.Logger.Info("Running command.", zap.String(base.LM_COMMAND, cmd.String()))

		Current = id
		PendingErr = nil
		PendingErr = c.Run()
		if PendingErr != nil {
			PendingErr = base.NewGinfraErrorChildA("Execution failed.", PendingErr, zap.String(base.LM_STDOUT,
				sout.String()), zap.String(base.LM_STDERR, serr.String()))
			Status = StatusError
		} else {
			Status = StatusDone
		}
		Current = ""
	}(cmd)

	// A slight delay to see if it fails fast.
	time.Sleep(ExecFastFailDelay * time.Millisecond)
	if PendingErr != nil {
		return PendingErr
	}
	return nil
}

// PostSeleniumGiseleniumRun (GET /selenium/run)
func (gis *Serviceservice_selenium) PostSeleniumGiseleniumRun(ctx echo.Context, params models.PostSeleniumGiseleniumRunParams) error {
	RunLock.Lock()
	defer RunLock.Unlock()

	var (
		err error
	)

	// Only run one at a time for now.
	if Status == StatusRunning {
		return local.PostErrorResponse(ctx, http.StatusConflict, base.NewGinfraErrorA("Already running something",
			base.LM_ID, Current))
	}

	if params.Target == "" {
		return local.PostErrorResponse(ctx, http.StatusBadRequest, errors.New("target is required"))
	}

	d, err := io.ReadAll(ctx.Request().Body)

	// Get executable
	id, _, err := processCache(gis, string(d))
	if err != nil {
		return local.PostErrorResponse(ctx, http.StatusInternalServerError, err)
	}

	err = runExecutable(gis, id, params.Target)
	if err != nil {
		return local.PostErrorResponse(ctx, http.StatusInternalServerError, err)
	}

	return ctx.JSON(http.StatusOK, models.SeleniumInfo{
		Id: &id,
	})
}

// === STATUS ========================================================================================

// GetSeleniumGiseleniumStatus (GET /selenium/status)
func (gis *Serviceservice_selenium) GetSeleniumGiseleniumStatus(ctx echo.Context, params models.GetSeleniumGiseleniumStatusParams) error {
	RunLock.Lock()
	defer RunLock.Unlock()

	// Id is ignored for now.
	e := ""
	if PendingErr != nil {
		e = PendingErr.Error()
	}
	status := &models.SeleniumStatus{
		Id:        &Current,
		Status:    &Status,
		LastError: &e,
	}
	return ctx.JSON(http.StatusOK, status)
}

// === STOP ==========================================================================================

// GetSeleniumGiseleniumStopId (GET /selenium/stop/{id})
func (gis *Serviceservice_selenium) GetSeleniumGiseleniumStopId(ctx echo.Context, id string) error {
	RunLock.Lock()
	defer RunLock.Unlock()

	// Id is ignored for now.
	if Status == StatusRunning {
		err := cmd.Process.Signal(syscall.SIGTERM)
		Status = StatusIdle
		PendingErr = nil
		if err != nil {
			return local.PostErrorResponse(ctx, http.StatusInternalServerError, err)
		}
	}

	return ctx.JSON(http.StatusOK, models.SeleniumInfo{
		Id: &id,
	})
}

// === LOG ==========================================================================================

// GetSeleniumGiseleniumLog (GET /giselenium/log)
func (gis *Serviceservice_selenium) GetSeleniumGiseleniumLog(ctx echo.Context, params models.GetSeleniumGiseleniumLogParams) error {
	RunLock.Lock()
	defer RunLock.Unlock()

	id := params.Id
	if id == "" {
		id = Current
	}
	if id == "" {
		return local.PostErrorResponse(ctx, http.StatusBadRequest, errors.New("cannot determine ID"))
	}

	lf := filepath.Join(gis.ServiceContext.Scfg.ServiceHome, CompileCachePath, id+".log")
	_, err := os.Stat(lf)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return local.PostErrorNotFoundResponse(ctx, err, "No Log file for "+id)
		} else {
			return local.PostErrorResponse(ctx, http.StatusInternalServerError, err)
		}
	}

	d, err := os.ReadFile(lf)
	if err != nil {
		return local.PostErrorResponse(ctx, http.StatusInternalServerError, err)
	}
	ds := string(d)

	log := &models.SeleniumLog{
		Id:     &id,
		Log:    &ds,
		Status: &Status,
	}
	return ctx.JSON(http.StatusOK, log)
}

// === LOG ==========================================================================================

// GetSeleniumGiseleniumResult (GET /giselenium/result)
func (gis *Serviceservice_selenium) GetSeleniumGiseleniumResult(ctx echo.Context, params models.GetSeleniumGiseleniumResultParams) error {
	RunLock.Lock()
	defer RunLock.Unlock()

	id := params.Id
	if id == "" {
		id = Current
	}
	if id == "" {
		return local.PostErrorResponse(ctx, http.StatusBadRequest, errors.New("cannot determine ID"))
	}

	lf := filepath.Join(gis.ServiceContext.Scfg.ServiceHome, CompileCachePath, id+".json")
	_, err := os.Stat(lf)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return local.PostErrorNotFoundResponse(ctx, err, "No result file for "+id)
		} else {
			return local.PostErrorResponse(ctx, http.StatusInternalServerError, err)
		}
	}

	d, err := os.ReadFile(lf)
	if err != nil {
		return local.PostErrorResponse(ctx, http.StatusInternalServerError, err)
	}
	ds := string(d)

	log := &models.SeleniumResult{
		Result: &ds,
	}

	if Status == StatusDone {
		Status = StatusIdle
	}

	return ctx.JSON(http.StatusOK, log)
}
