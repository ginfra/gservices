/*
Package service
// >A#

You may license this file however you wish.

// >A#
*/
package service

import (
	"github.com/getkin/kin-openapi/openapi3"
	"github.com/labstack/echo/v4"
	"gitlab.com/ginfra/ginfra/common/service/shared"
	"gitlab.com/ginfra/gservices/service_selenium/api/service/server"
	"gitlab.com/ginfra/gservices/service_selenium/local"
	"gitlab.com/ginfra/gservices/service_selenium/providers"
	// # YOUR IMPORTS START HERE
	// >B###############################################################################################################
	// >B###############################################################################################################
	// #  YOUR IMPORTS END
)

type Serviceservice_selenium struct {
	Providers      *providers.ProvidersLoaded
	ServiceContext *local.GContext

	// # ADD ADDITIONAL SERVICE DATA START HERE
	// >C###############################################################################################################

	// >C###############################################################################################################
	// # ADD ADDITIONAL SERVICE DATA END HERE
}

func NewServiceservice_selenium(gcontext *local.GContext, providers *providers.ProvidersLoaded) (*Serviceservice_selenium, error) {
	return &Serviceservice_selenium{
		Providers:      providers,
		ServiceContext: gcontext,
	}, nil
}

func ServiceSetup(service *Serviceservice_selenium) error {

	var err error

	// # ADD ADDITIONAL SETUP CODE START HERE.  All configuration is complete and the providers are ready.
	// >D###############################################################################################################

	err = runSetup(service)

	// >D###############################################################################################################
	// # ADD ADDITIONAL SETUP CODE END HERE

	return err
}

var specifics string

func getServiceSpecifics(gis *Serviceservice_selenium) string {
	if specifics == "" {
		specifics = shared.LoadSpecifics(gis.ServiceContext.Scfg.ServiceHome)
	}
	return specifics
}

func RegisterHandlers(echoServer *echo.Echo, service *Serviceservice_selenium) ([]*openapi3.T, error) {

	// Always register control.
	sw, err := server.GetSwagger()
	s := []*openapi3.T{sw}
	s = append(s, sw)
	server.RegisterHandlers(echoServer, service)

	// NOTE: if you merge all services into one, the above registration is all you need.
	//
	// If you have multiple api services you host will need to be registered here.
	// If you have just a single api server, this should be enough:
	//   service.RegisterHandlers(echoServer, service)
	//
	// If you have more than one, you'll need to import them with unique names like this:
	//    v1 "gitlab.com/ginfra/gservices/service_otherservice/api/service_otherservice/v1/server"
	//    v2 "gitlab.com/ginfra/gservices/service_otherservice/api/service_otherservice/v2/server"
	//
	// and register them something like this:
	//   v1.RegisterHandlers(echoServer, service)
	//   v2.RegisterHandlers(echoServer, service)
	//
	// If you want schema validation, you must add the swagger definition to the array returned by this function.
	// It could look like this for a single api:
	//
	//      sw, err := server.GetSwagger()
	//      s = append(s, sw)
	//
	// Or this for multiple apis.
	//
	// 		sw, err := v1.GetSwagger()
	//		s = append(s, sw)
	//		sw, err = v2.GetSwagger()
	//		s = append(s, sw)
	//
	// #  REGISTRATIONS GO HERE
	// >E###############################################################################################################

	// >E###############################################################################################################
	// # END REGISTRATIONS

	return s, err
}

// ^^###################################################################################################################
// # ADDITIONAL CODE GOES BELOW HERE
// # Stubs can go here or in other files.
