#!/usr/bin/env bash

##########################################################################
# Place custom entry provision tasks performed during docker build here
# This file will not be updated, so any additions or changes are safe.

echo "Custom provisioning unique to this service."

sudo wget https://download.oracle.com/java/22/latest/jdk-22_linux-x64_bin.deb
sudo dpkg -i jdk-22_linux-x64_bin.deb
sudo rm -f jdk-22_linux-x64_bin.deb

