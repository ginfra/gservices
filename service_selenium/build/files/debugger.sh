#!/usr/bin/env bash

DPORT=$(<DEBUGGER)

./debugging.sh
wget https://go.dev/dl/go1.22.4.linux-amd64.tar.gz
rm -rf /usr/local/go && tar -C /usr/local -xzf go1.22.4.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
go install github.com/go-delve/delve/cmd/dlv@latest
cd /service
/root/go/bin/dlv exec /service/ginfra_service.exe --log --wd /service --listen "0.0.0.0:${DPORT}" --headless --api-version=2 --accept-multiclient -- /service

