#!/usr/bin/env bash

# ARG1 Name
# ARG2 Classpath
# ARG3 Class
# ARG4 Host
# ARG5 Target

echo "NAME      : ${1}" > /service/store/cache/${1}.log
echo "CLASSPATH : ${2}" >> /service/store/cache/${1}.log
echo "CLASS     : ${3}" >> /service/store/cache/${1}.log
echo "HOST      : ${4}" >> /service/store/cache/${1}.log
echo "TARGET    : ${5}" >> /service/store/cache/${1}.log

java -classpath ${2} ${3} ${4} ${5} "/service/store/cache/${1}" < /dev/null >> /service/store/cache/${1}.log 2>&1

