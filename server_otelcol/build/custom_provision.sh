#!/usr/bin/env bash

##########################################################################
# Place custom entry provision tasks performed during docker build here
# This file will not be updated, so any additions or changes are safe.

echo "Custom provisioning unique to this service."

# Install the collector.
apt-get update
apt-get -y install wget systemctl
wget https://github.com/open-telemetry/opentelemetry-collector-releases/releases/download/v0.100.0/otelcol_0.100.0_linux_amd64.deb
dpkg -i otelcol_0.100.0_linux_amd64.deb
rm -f otelcol_0.100.0_linux_amd64.deb

systemctl stop otelcol.service
