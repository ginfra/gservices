#!/usr/bin/env bash

########################################################################################
# Start the server

systemctl stop otelcol.service
systemctl start otelcol.service

ls -la /run | grep -q otelcol ; export START_STATUS="$?"

if [ "$START_STATUS" != "0" ]; then
    echo "OTELCOL did not start." >> /service/log/start.log
fi

exit "$START_STATUS"
