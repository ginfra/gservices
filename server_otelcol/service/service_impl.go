/*
Package service
// >A#

You may license this file however you wish.

// >A#
*/
package service

import (
	"bytes"
	"github.com/getkin/kin-openapi/openapi3"
	"github.com/labstack/echo/v4"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common/service/shared"
	"gitlab.com/ginfra/gservices/server_otelcol/api/service/server"
	"gitlab.com/ginfra/gservices/server_otelcol/local"
	"gitlab.com/ginfra/gservices/server_otelcol/providers"
	"go.uber.org/zap"
	"os"
	"os/exec"
	// # YOUR IMPORTS START HERE
	// >B###############################################################################################################
	// >B###############################################################################################################
	// #  YOUR IMPORTS END
)

type Serviceserver_otelcol struct {
	Providers      *providers.ProvidersLoaded
	ServiceContext *local.GContext

	// # ADD ADDITIONAL SERVICE DATA START HERE
	// >C###############################################################################################################

	// >C###############################################################################################################
	// # ADD ADDITIONAL SERVICE DATA END HERE
}

func NewServiceserver_otelcol(gcontext *local.GContext, providers *providers.ProvidersLoaded) (*Serviceserver_otelcol, error) {
	return &Serviceserver_otelcol{
		Providers:      providers,
		ServiceContext: gcontext,
	}, nil
}

func ServiceSetup(service *Serviceserver_otelcol) error {

	var err error

	// # ADD ADDITIONAL SETUP CODE START HERE.  All configuration is complete and the providers are ready.
	// >D###############################################################################################################

	err = startServer(service)

	// >D###############################################################################################################
	// # ADD ADDITIONAL SETUP CODE END HERE

	return err
}

var specifics string

func getServiceSpecifics(gis *Serviceserver_otelcol) string {
	if specifics == "" {
		specifics = shared.LoadSpecifics(gis.ServiceContext.Scfg.ServiceHome)
	}
	return specifics
}

func RegisterHandlers(echoServer *echo.Echo, service *Serviceserver_otelcol) ([]*openapi3.T, error) {

	// Always register control.
	sw, err := server.GetSwagger()
	s := []*openapi3.T{sw}
	s = append(s, sw)
	server.RegisterHandlers(echoServer, service)

	// NOTE: if you merge all services into one, the above registration is all you need.
	//
	// If you have multiple api services you host will need to be registered here.
	// If you have just a single api server, this should be enough:
	//   service.RegisterHandlers(echoServer, service)
	//
	// If you have more than one, you'll need to import them with unique names like this:
	//    v1 "gitlab.com/ginfra/gservices/service_otherservice/api/service_otherservice/v1/server"
	//    v2 "gitlab.com/ginfra/gservices/service_otherservice/api/service_otherservice/v2/server"
	//
	// and register them something like this:
	//   v1.RegisterHandlers(echoServer, service)
	//   v2.RegisterHandlers(echoServer, service)
	//
	// If you want schema validation, you must add the swagger definition to the array returned by this function.
	// It could look like this for a single api:
	//
	//      sw, err := server.GetSwagger()
	//      s = append(s, sw)
	//
	// Or this for multiple apis.
	//
	// 		sw, err := v1.GetSwagger()
	//		s = append(s, sw)
	//		sw, err = v2.GetSwagger()
	//		s = append(s, sw)
	//
	// #  REGISTRATIONS GO HERE
	// >E###############################################################################################################

	// >E###############################################################################################################
	// # END REGISTRATIONS

	return s, err
}

// ^^###################################################################################################################
// # ADDITIONAL CODE GOES BELOW HERE
// # Stubs can go here or in other files.

func startServer(service *Serviceserver_otelcol) error {
	data, err := service.ServiceContext.ConfigProvider.StoreGet("config.yaml", service.ServiceContext.Scfg.DeploymentName,
		service.ServiceContext.Scfg.Name, false)
	if err == nil {
		err = os.WriteFile("/etc/otelcol/config.yaml", data, 0644)
		if err != nil {
			local.Logger.Error("Could not write downloaded config.yaml file..", zap.Error(err))
		}
	} else {
		local.Logger.Warn("Did not get new config.yaml from configuration store.  Using default.")
		local.Logger.Debug("...Error message when trying to get config.yaml.", zap.Error(err))
	}

	// Start service
	cmd := exec.Command("bash", "/start_server.sh")
	var sout bytes.Buffer
	cmd.Stdout = &sout
	var serr bytes.Buffer
	cmd.Stdout = &serr

	err = cmd.Run()
	if err != nil {
		local.Logger.Error("Could not start otelcol.service.", zap.Error(err), zap.String(base.LM_STDOUT, sout.String()),
			zap.String(base.LM_STDERR, serr.String()))
	}

	return err
}
