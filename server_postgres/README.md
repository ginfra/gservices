# Service implementation

### This file will not be updated, so any additions or changes are safe.

## Service information

... custom to your service...

## Specifics

[Specifics](doc/Specifics.md) is the mechanism where the service can tell GInfra information about how it should be run.
See the [additional documentation](doc/Specifics) for more information.

## File layout
| Directory/File      | Purpose                                                                                                                           |
|---------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| api/                | Generated service stubs and code.                                                                                                 | 
| build/              | Target build directory and container build files.                                                                                 |
| doc/                | Additional documentation.                                                                                                         |
| local/              | Data, values and tools specific to your service.                                                                                  |
| providers/          | Service providers that are unique to your service.                                                                                |
| service/            | Your service implementation.  This is where you will implement the interfaces defined in your API specification and tests for it. |
| service/private.go  | Server overhead.  Generally you can leave it alone.                                                                               |
| test/               | Data and configuration for local testing.                                                                                         |
| .meta.json          | Service description metafile.  This will be configered during service generation.                                                 |
| service_config.json | Dynamic configuration file.  This one is provided purely for testing purposes.                                                    |
| start_local.cmd     | Start the service as a local executable (windows)                                                                                 |
| start_local.sh      | Start the service as a local executable (linux/osx/etc)                                                                           |
| taskfile.yaml       | Default task target file.  See below.                                                                                             |


### Note about start_local.*

While you can run these yourself, they are meant to be started with the 'ginfra manage local
start' command.  It will handle any dependencies and configuration.

## Developer information

For automated tasks that build and do other actions, see the [task](doc/TASK.md) document.

## Starting postgres container for test purposes.

This will start the server but not the control (and other) service.

```docker run -d -p 5432:5432 gservices/server_postgres NOSERVICE```

You can then get the postgres connection URL for the postgres user with the following:

```docker exec -it CONTAINERID cat /postgres_url```

You can get the CONTAINERID with by running ```docker ps```.

## Running the container as a hosted for test purposes.

Run the following two commands:

```
ginfra manage host init gservices/giconfig
ginfra manage host start postgres gservices/server_postgres --cmd NOSERVICE
```

This will start the container without starting the ginfra service.  You can exec into the container
and start the service with the following command:

```
cd service
./ginfra_service /service
```


