#!/usr/bin/env bash

###########################################
# Place custom entry tasks here to be done before starting the server.
# This file will not be updated, so any additions or changes are safe.

# Change password for postgres user.
export NEW_PASSWORD=$(date +%s | sha256sum | base64 | head -c 16)
echo "${NEW_PASSWORD}" > /postgres_password

echo "postgres://postgres:${NEW_PASSWORD}@localhost:5432" > /postgres_url

su -c '/setup_db.sh' postgres
