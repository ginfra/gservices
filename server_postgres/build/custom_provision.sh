#!/usr/bin/env bash

##########################################################################
# Place custom entry provision tasks performed during docker build here
# This file will not be updated, so any additions or changes are safe.

echo "Custom provisioning unique to this service."

apt update -y
apt install -y wget gnupg2

addgroup --system --gid 500 postgres; \
    adduser --system --ingroup postgres --uid 500 --home /home/postgres --shell /bin/bash postgres; \
    chown -R postgres:postgres /home/postgres

wget -q -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | gpg --dearmor -o /etc/apt/trusted.gpg.d/postgresql.gpg
sh -c 'echo "deb https://apt.postgresql.org/pub/repos/apt bookworm-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
apt update -y
apt install -y postgresql-16 sudo

cp -f /etc/postgresql/16/main/postgresql.conf /etc/postgresql/16/main/postgresql.conf.OLD
cp -f /etc/postgresql/16/main/pg_hba.conf  /etc/postgresql/16/main/pg_hba.conf.OLD
cp -f /postgresql.conf /etc/postgresql/16/main/postgresql.conf.NEW
cp -f /postgresql.conf /etc/postgresql/16/main/postgresql.conf

pg_ctlcluster 16 main start

cp -f /pg_hba.conf /etc/postgresql/16/main/pg_hba.conf.NEW
cp -f /pg_hba.conf /etc/postgresql/16/main/pg_hba.conf

pg_ctlcluster 16 main stop

rm -f install.sh
