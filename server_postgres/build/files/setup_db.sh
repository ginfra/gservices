#!/usr/bin/env bash

###########################################
# Setup db

NEW_PASSWORD=$(cat /postgres_password)
psql -U postgres -c "ALTER USER postgres WITH PASSWORD '${NEW_PASSWORD}'";
