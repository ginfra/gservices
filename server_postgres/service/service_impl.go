/*
Package service
// >A#

You may license this file however you wish.

// >A#
*/
package service

import (
	"github.com/getkin/kin-openapi/openapi3"
	"github.com/labstack/echo/v4"
	"gitlab.com/ginfra/ginfra/common/service/shared"
	"gitlab.com/ginfra/gservices/server_postgres/api/service/server"
	"gitlab.com/ginfra/gservices/server_postgres/local"
	"gitlab.com/ginfra/gservices/server_postgres/providers"
	// # YOUR IMPORTS START HERE
	// >B###############################################################################################################

	"context"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common"
	"go.uber.org/zap"
	"os"
	"path"
	"strings"
	"sync"
	// >B###############################################################################################################
	// #  YOUR IMPORTS END
)

type Serviceserver_postgres struct {
	Providers      *providers.ProvidersLoaded
	ServiceContext *local.GContext

	// # ADD ADDITIONAL SERVICE DATA START HERE
	// >C###############################################################################################################

	ServiceLock           sync.Mutex
	PostgresAdminPassword string
	ServerPort            int
	ServerUrl             string
	MyIp                  string

	// >C###############################################################################################################
	// # ADD ADDITIONAL SERVICE DATA END HERE
}

func NewServiceserver_postgres(gcontext *local.GContext, providers *providers.ProvidersLoaded) (*Serviceserver_postgres, error) {
	return &Serviceserver_postgres{
		Providers:      providers,
		ServiceContext: gcontext,
	}, nil
}

func ServiceSetup(service *Serviceserver_postgres) error {

	var err error

	// # ADD ADDITIONAL SETUP CODE START HERE.  All configuration is complete and the providers are ready.
	// >D###############################################################################################################

	// Connection to the db itself can be provided by either specifics or local file.  It will check specifics first.
	var (
		puri string
		d    []byte
	)

	// Might be set by other means, which will take priority.
	if service.ServerUrl == "" {

		scfg := local.GetConfiguredServiceSpecifics(service.ServiceContext)
		if scfg.Err == nil {
			puri, err = scfg.GetValue(local.ConfigPostgressUrl).GetValueString()
		} else {
			err = scfg.Err
			local.Logger.Warn("Could not get URL from config specifics.  Trying file.", zap.String(base.LM_CAUSE, err.Error()))
		}

		// See if it is in a file.
		if puri == "" {
			d, err = os.ReadFile(path.Join(service.ServiceContext.Scfg.ServiceHome, local.ConfigPostgressUrl))
			if err != nil {
				local.Logger.Warn("Could not open URL file in service directory.  Trying root.")
				d, err = os.ReadFile(path.Join("/", local.ConfigPostgressUrl))
			}
			if err == nil {
				service.ServerUrl = strings.TrimSpace(string(d))
			} else {
				return base.NewGinfraErrorChildA("Unable to determine server url.", err)
			}

		}

	}

	local.Logger.Info("URL: " + service.ServerUrl)

	service.MyIp = common.GetOutboundIP()

	// >D###############################################################################################################
	// # ADD ADDITIONAL SETUP CODE END HERE

	return err
}

var specifics string

func getServiceSpecifics(gis *Serviceserver_postgres) string {
	if specifics == "" {
		specifics = shared.LoadSpecifics(gis.ServiceContext.Scfg.ServiceHome)
	}
	return specifics
}

func RegisterHandlers(echoServer *echo.Echo, service *Serviceserver_postgres) ([]*openapi3.T, error) {

	// Always register control.
	sw, err := server.GetSwagger()
	s := []*openapi3.T{sw}
	s = append(s, sw)
	server.RegisterHandlers(echoServer, service)

	// NOTE: if you merge all services into one, the above registration is all you need.
	//
	// If you have multiple api services you host will need to be registered here.
	// If you have just a single api server, this should be enough:
	//   service.RegisterHandlers(echoServer, service)
	//
	// If you have more than one, you'll need to import them with unique names like this:
	//    v1 "gitlab.com/ginfra/gservices/service_otherservice/api/service_otherservice/v1/server"
	//    v2 "gitlab.com/ginfra/gservices/service_otherservice/api/service_otherservice/v2/server"
	//
	// and register them something like this:
	//   v1.RegisterHandlers(echoServer, service)
	//   v2.RegisterHandlers(echoServer, service)
	//
	// If you want schema validation, you must add the swagger definition to the array returned by this function.
	// It could look like this for a single api:
	//
	//      sw, err := server.GetSwagger()
	//      s = append(s, sw)
	//
	// Or this for multiple apis.
	//
	// 		sw, err := v1.GetSwagger()
	//		s = append(s, sw)
	//		sw, err = v2.GetSwagger()
	//		s = append(s, sw)
	//
	// #  REGISTRATIONS GO HERE
	// >E###############################################################################################################

	// >E###############################################################################################################
	// # END REGISTRATIONS

	return s, err
}

// ^^###################################################################################################################
// # ADDITIONAL CODE GOES BELOW HERE
// # Stubs can go here or in other files.

func performInit(gis *Serviceserver_postgres, name string) (string, error) {
	var (
		err error
	)

	//url := fmt.Sprintf("postgres://postgres:%s@%s:%d/", gis.PostgresAdminPassword, gis.MyIp, local.POSTGRES_PORT)
	// Yes I know this isn't the best way to connect for security.  Remember, this service is for demonstration.
	var dbpool *pgxpool.Pool
	dbpool, err = pgxpool.New(context.Background(), gis.ServerUrl)
	if err != nil {
		return "", base.NewGinfraErrorChild("Unable to create database connection.", err)
	}
	defer dbpool.Close()

	//var tag pgconn.CommandTag
	p := base.GeneratePassword(base.COMMON_PASSWORD_LENGTH)
	_, err = dbpool.Exec(context.Background(), "CREATE USER "+name+" WITH PASSWORD '"+p+"';")
	if err != nil {
		return "", base.NewGinfraErrorChildA("Failed to add new user.", err, base.LM_NAME, name)
	}
	local.Logger.Info("User created.", zap.String(base.LM_NAME, name))

	sql := "CREATE DATABASE " + name + " WITH OWNER " + name
	local.Logger.Info("Database create sql.", zap.String(base.LM_NAME, sql))
	_, err = dbpool.Exec(context.Background(), "CREATE DATABASE "+name+" WITH OWNER "+name+";")
	if err != nil {
		return "", base.NewGinfraErrorChildA("Failed to create new database.", err, base.LM_NAME, name)
	}
	local.Logger.Info("Database created.", zap.String(base.LM_NAME, name))

	return p, err
}
