//go:build functional_test

package service

import (
	"gitlab.com/ginfra/ginfra/base"
	"testing"

	// # YOUR IMPORTS START HERE
	// >B###############################################################################################################

	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ginfra/gservices/server_postgres/api/service/client"
	"gitlab.com/ginfra/gservices/server_postgres/api/service/models"
	"gitlab.com/ginfra/gservices/server_postgres/local"
	"net/http"

	// >B###############################################################################################################
	// #  YOUR IMPORTS END
)

func yourSetup(t *testing.T, context *TestServiceContext) error {

	// # YOUR SETUP STARTS HERE
	// >C###############################################################################################################

	return nil

	// >C###############################################################################################################
	// #  YOUR SETUP ENDS HERE
}

func yourTeardown(t *testing.T, context *TestServiceContext) error {

	// # YOUR TEARDOWN STARTS HERE
	// >D###############################################################################################################

	return nil

	// >D###############################################################################################################
	// #  YOUR TEARDOWN ENDS HERE
}

/*
	IMPORTANT: Add the following two lines to the start of a Test* function.
		teardown, tcontext := setupTest(t, yourSetup)
		defer teardown(t, tcontext, yourTeardown)
*/
// ^^###################################################################################################################
// # ADDITIONAL CODE GOES BELOW HERE

func testInit(t *testing.T, c *client.ClientWithResponses) {

	var ir models.GiControlDatasourceInitJSONRequestBody
	dbn := base.GenerateCodeword(6)
	ir.Name = dbn

	local.Logger.Info("Database name is " + dbn)

	if response, err := c.GiControlDatasourceInitWithResponse(context.Background(), ir); err == nil {
		assert.Equal(t, http.StatusOK, response.StatusCode(), "Code")
		if response.StatusCode() == http.StatusOK {
			assert.Equal(t, response.JSON200.Username, dbn, "Username")
			local.Logger.Info("New DB uri: " + response.JSON200.Uri)
		}
		/*
			Not really needed.
			Password *string `json:"password,omitempty"`
			Port     *int    `json:"port,omitempty"`
		*/
	} else {
		t.Error(fmt.Errorf("Bad response.  %v", err))
	}

}

func TestServicePostgress(t *testing.T) {
	teardown, tcontext := setupTest(t, yourSetup)
	defer teardown(t, tcontext, yourTeardown)

	//s, e := performInit(tcontext.service, "BORK")
	//fmt.Sprintf("string: %s, error: %v", s, e)

	lc, err := client.NewClientWithResponses("http://localhost:8900")
	if err != nil {
		t.Error(fmt.Errorf("Could not get client.  %v", err))
		return
	}

	testInit(t, lc)

}
