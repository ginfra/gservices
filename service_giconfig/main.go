/*
Package main
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Service entrypoint.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package main

import (
	"fmt"
	"gitlab.com/ginfra/gservices/service_giconfig/local"
	"gitlab.com/ginfra/gservices/service_giconfig/service"
	"go.uber.org/zap"
	"net/http"
	"os"
)

func main() {

	var (
		c   *local.GContext
		err error
	)

	// Get the command values.
	if err = local.RootCmd.Execute(); err != nil {
		panic(fmt.Sprintf("Bad command: %v", err))
	}

	// Get context and setup
	if c, err = local.GetContext(); err != nil || c == nil {
		panic(fmt.Sprintf("Failed to configure service context: %v", err))
	}
	if c.Serr != nil {
		panic(fmt.Sprintf("Failed to configure service context: due to context configuration. %v", c.Serr))
	}
	s := service.Setup(c)

	// Run it
	err = service.RunService(s)
	if err != http.ErrServerClosed {
		fmt.Printf("Server exited with an error: %v", err)
		if local.Logger != nil {
			local.Logger.Fatal("Server exited with an error.", zap.Error(err))
		}
	}
	os.Exit(0)
}
