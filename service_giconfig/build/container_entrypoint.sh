#!/usr/bin/env bash

###########################################
#

echo "Starting container" >> /service/log/start.log

ls -la /config | true

if [ "$1" == "DOCS" ]; then
  cat /UserDocs.md
  exit 0
fi

if [ -f "/service_config.json" ]; then
  mv -f /service_config.json /service/service_config.json

fi

if [ -f "/config/service_config.json" ]; then
  cp /config/service_config.json /service/service_config.json
fi

if [ -f "DEBUGGER" ]; then
  DPORT=$(<DEBUGGER)

    echo "Debugging turned on." >> /service/log/start.log
  ./debugging.sh
  wget https://go.dev/dl/go1.22.4.linux-amd64.tar.gz
  rm -rf /usr/local/go && tar -C /usr/local -xzf go1.22.4.linux-amd64.tar.gz
  export PATH=$PATH:/usr/local/go/bin
  go install github.com/go-delve/delve/cmd/dlv@latest
  cd /service
  /root/go/bin/dlv exec /service/ginfra_service.exe --log --wd /service --listen "0.0.0.0:${DPORT}" --headless --api-version=2 --accept-multiclient -- /service

else

  cd /service
    echo "Execute ginfra_service" >> /service/log/start.log
  ./ginfra_service.exe /service $1 $2 $3 $4 $5 $6 $7 $8 $9 >> /service/log/start.log 2>&1

fi

cat /service/log/start.log

