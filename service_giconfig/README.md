# Service implementation

## IMPORTANT NOTICE!!!

The config service is a custom service.  Since it doesn't participate in the same configuration provider
scheme as every other service (since it IS the provider), it has to be modified.  DO NOT use the 
ginfra update command against this service.  IT WILL MESS IT UP.

## Implementation

The current implementation simply uses the file based configuration provider that is part of the
ginfra/common/config package.  The configuration is in-memory and with every update to the config, 
the data is written to disk, so it isn't a particularly robust service.   However, since
configuration changes are relatively rare, it should be a suitable solution for now.  And before you
make too much fun of it, keep in mind that this is pretty much how Redis works.

### File layout
| Directory/File     | Purpose                                                                                                                           |
|--------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| api/               | Generated service stubs and code.                                                                                                 | 
| local/             | Data, values and tools specific to your service.                                                                                  |
| providers/         | Service providers that are unique to your service.                                                                                |
| service/           | Your service implementation.  This is where you will implement the interfaces defined in your API specification and tests for it. |
| service/private.go | Server overhead.  Generally you can leave it alone.                                                                               |
| test/              | Data and configuration for local testing.                                                                                         |


## Start up


