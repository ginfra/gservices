/*
Package service
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Private implementation.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package service

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/deepmap/oapi-codegen/pkg/middleware"
	"github.com/getkin/kin-openapi/openapi3"
	"github.com/labstack/echo/v4"
	echomiddleware "github.com/labstack/echo/v4/middleware"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common/config"
	"gitlab.com/ginfra/ginfra/common/log"
	gecho "gitlab.com/ginfra/ginfra/common/service/echo"
	"gitlab.com/ginfra/gservices/service_giconfig/api/service_giconfig/server"
	"gitlab.com/ginfra/gservices/service_giconfig/local"
	"gitlab.com/ginfra/gservices/service_giconfig/providers"
	"go.uber.org/zap"
	"net"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"testing"
	"time"
)

// #####################################################################################################################
// # SERVER

var (
	echoServer      *echo.Echo = nil
	testServiceHome            = flag.String("test_service_home", "", "")
)

func clearTheStore(c *local.GContext) {
	fmt.Println("Ordered to clear the store.")

	// The config data files we will enumerate for debugging.
	f, err := filepath.Glob(filepath.Join(c.ServiceHome, local.CSF_STORE_DIRECTORY, config.CONFIG_FILE_PREFIX+"*"))
	if err != nil {
		panic(err)
	}
	for _, file := range f {
		fmt.Printf("....removing: %v\n", file)
		if err = os.Remove(file); err != nil {
			panic(err)
		}
	}
}

func ginfraHTTPErrorHandler(err error, c echo.Context) {

	if c.Response().Committed {
		return
	}

	he, ok := err.(*echo.HTTPError)
	if ok {
		if he.Internal != nil {
			if herr, ok := he.Internal.(*echo.HTTPError); ok {
				he = herr
			}
		}
	} else {
		he = &echo.HTTPError{
			Code:    http.StatusInternalServerError,
			Message: http.StatusText(http.StatusInternalServerError),
		}
	}

	code := he.Code
	message := he.Message

	switch m := he.Message.(type) {
	case string:
		message = echo.Map{"message": m}
	case json.Marshaler:
		// do nothing - this type knows how to format itself to JSON
	case error:
		message = echo.Map{"message": m.Error()}
	}

	// Send response
	if c.Request().Method == http.MethodHead { // Issue #608
		err = c.NoContent(he.Code)
	} else {
		err = c.JSON(code, message)
	}
	if err != nil {
		local.Logger.Error("Http Error", zap.Error(err))
	}
}

func Setup(c *local.GContext) *Servicegiconfig {

	var (
		pl  *providers.ProvidersLoaded
		err error = nil
	)

	// Normalize serviceHome
	if i := strings.Index(c.ServiceHome, ":"); i > 0 {
		// Mostly because I have to run this on windows too.
		c.ServiceHome = c.ServiceHome[i+1:]
		c.ServiceHome = strings.ReplaceAll(c.ServiceHome, "\\", "/")
	}

	// Clear the config data from the store?
	if c.Clear == true {
		clearTheStore(c)
	}

	base.RegisterService(local.ServiceName) // This must happen because it might be used by the other setup.
	if err = base.SetupValues(); err != nil {
		panic(fmt.Sprintf("Failed to basic values setup: %v", err))
	}

	// Logger
	local.Logger = log.GetLogger(path.Join(c.ServiceHome, base.LMCONFIG_LOGDIR), true)

	// Providers
	if pl, err = providers.ProvidersLoad(c); err != nil {
		panic(fmt.Sprintf("Failed to load providers: %v", err))
	}

	// We need to have the provider loaded to clear the file store.
	if c.Clear == true {
		err = pl.ConfigStore.Cp.ClearStore()
		local.Logger.Warn("Could not clear the file store.", zap.Error(err))
	}

	// Create the service context
	service, errr := NewServicegiconfig(c, pl)
	if errr != nil {
		panic(errr)
	}

	// Additional setup.
	if err = ServiceSetup(service); err != nil {
		panic(fmt.Sprintf("Failed to setup service: %v", err))
	}

	local.Logger.Info("Service configured.", zap.String(base.LM_SERVICE_NAME, local.ServiceName))
	return service
}

func RunService(service *Servicegiconfig) error {
	var (
		err     error = nil
		swagger *openapi3.T
	)

	if echoServer != nil {
		panic("Only one server may run per app instance.")
	}

	if swagger, err = server.GetSwagger(); err == nil {
		swagger.Servers = nil

		echoServer = echo.New()

		// Logging.
		echoServer.Use(gecho.Echo2ZapLogger(local.Logger))
		echoServer.HTTPErrorHandler = ginfraHTTPErrorHandler

		// TODO Be careful with this.  We will need to fixup with actual values.
		echoServer.Use(echomiddleware.CORSWithConfig(echomiddleware.CORSConfig{
			AllowOrigins: []string{"*"},
			AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete, http.MethodHead},
		}))

		echoServer.Use(middleware.OapiRequestValidator(swagger))

		server.RegisterHandlers(echoServer, service)

		local.Logger.Info("Service starting.", zap.String(base.LM_SERVICE_NAME, local.ServiceName))

		// TODO the config service does not get it's hostname from ginfra since it does not load
		// a config.  Might need to have a way to pass in the host name.
		// Convert loopback or localhost to open ip.
		hostname := service.ServiceContext.Hostname
		if hostname == "127.0.0.1" || hostname == "localhost" {
			hostname = "0.0.0.0"
		}
		err = echoServer.Start(fmt.Sprintf("%v:%v", hostname, config.RESERVED_CONFIG_SERVICE_PORT))
	}
	return err
}

func StopService() error {
	if echoServer == nil {
		panic("Service not running.")
	}
	local.Logger.Info("Stopping service.", zap.String(base.LM_SERVICE_NAME, local.ServiceName))

	// Taken from the cookbook.
	//quit := make(chan os.Signal)
	//signal.Notify(quit, os.Interrupt)
	//<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err := echoServer.Shutdown(ctx)

	echoServer = nil
	return err
}

// #####################################################################################################################
// # TESTING

type TestServiceContext struct {
	named            map[string]string
	testServiceHome  string
	adminConfigToken string
	gctx             *local.GContext
}

func setupTest(t *testing.T, userSetup func(t *testing.T, context *TestServiceContext) error) (
	func(t *testing.T, context *TestServiceContext, teardown func(t *testing.T, context *TestServiceContext) error),
	*TestServiceContext) {

	var (
		tcontext TestServiceContext
		err      error
	)

	// The debugger is being a real pain about this.
	if *testServiceHome == "" {
		sh, _ := os.Getwd()
		testServiceHome = &sh
	}

	// Fake context
	gctx := local.GContext{
		ServiceHome:      *testServiceHome,
		ConfigStoreUri:   "file://test/config.json",
		AdminConfigToken: config.AUTH_ADMIN_TOKEN_DEFAULT,
		InstanceType:     config.ITLocal,
	}
	local.GlobalGContext = &gctx
	clearTheStore(&gctx)

	err = userSetup(t, &tcontext) //HAX  Need to delete the files before the server starts
	service := Setup(&gctx)
	tcontext.testServiceHome = *testServiceHome
	tcontext.adminConfigToken = gctx.AdminConfigToken
	tcontext.gctx = &gctx

	if err != nil {
		panic(fmt.Sprintf("SETUP FAILED: %v", err))
	}

	// Run it
	cerr := make(chan error)
	if err == nil {
		go func() {
			cerr <- RunService(service)
		}()

		// Wait for server.
		hostname, _ := os.Hostname()
		attempts := 0
		for {
			_, err := net.Dial("tcp", fmt.Sprintf(hostname+":%v", config.RESERVED_CONFIG_SERVICE_PORT))
			if err == nil {
				break
			}
			attempts++
			if attempts > 10 {
				panic("FAIL: Server did not start.")
			}
			time.Sleep(time.Second)
		}
	}

	// Return a function to teardown the test
	fn := func(t *testing.T, tcontext *TestServiceContext, teardown func(t *testing.T, c *TestServiceContext) error) {

		// Teardown errors are purely informational.
		err := teardown(t, tcontext)
		if err != nil {
			fmt.Printf("ERROR during teardown:  %v", err)
		}

		_ = StopService() // Just consume it for now.
		if errr := <-cerr; errr != nil && !errors.Is(errr, http.ErrServerClosed) {
			panic(fmt.Sprintf("Server closed with error.  %v", errr))
		}
	}

	return fn, &tcontext
}
