/*
Package service
Copyright (c) 2023 Erich Gatejen
[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt

Service control implementation.
*/
package service

import (
	"github.com/labstack/echo/v4"
	models "gitlab.com/ginfra/gservices/service_giconfig/api/service_giconfig/models"
	"net/http"
	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################
)

// == Default NOOP implementations.  Leave this alone if this service does not respond to control.
// == This file will not be updated so you can change these to whatever you want.

// Initialize a data source.
// (POST /giconfig/ginfra/datasource/init)
func (sc *Servicegiconfig) GiControlDatasourceInit(ctx echo.Context) error {
	_ = ctx.String(http.StatusOK, "OK")
	return nil
}

// Reset the service/server.
// (POST /giconfig/ginfra/manage/reset)
func (sc *Servicegiconfig) GiControlManageReset(ctx echo.Context, params models.GiControlManageResetParams) error {
	_ = ctx.String(http.StatusOK, "OK")
	return nil
}

// Stop the service/server.
// (POST /giconfig/ginfra/manage/stop)
func (sc *Servicegiconfig) GiControlManageStop(ctx echo.Context, params models.GiControlManageStopParams) error {
	_ = ctx.String(http.StatusOK, "OK")
	return nil
}

// Get specifics for this service
// (GET /specifics)
func (gis *Servicegiconfig) GiGetSpecifics(ctx echo.Context) error {
	var r = getServiceSpecifics(gis)
	return ctx.JSON(http.StatusOK, r)
}
