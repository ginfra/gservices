/*
Package service
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Tools.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package service

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/ginfra/ginfra/api/service_giconfig/models"
	"gitlab.com/ginfra/ginfra/base"
	"net/http"
)

func getErrorResponse(err error) models.ErrorResponse {
	m := err.Error()
	e := models.ErrorResponse{
		Message: &m,
	}
	return e
}

func postErrorResponse(ctx echo.Context, code int, err error) error {
	err = ctx.JSON(code, getErrorResponse(err))
	return err
}

func postErrorUnauthorizedResponse(ctx echo.Context, path string, err error) error {
	err = ctx.JSON(http.StatusUnauthorized, getErrorResponse(base.NewGinfraErrorChildA("Not authorized.", err,
		base.LM_CONFIG_PATH, path)))
	return err
}

func postErrorNotFoundResponse(ctx echo.Context, err error, target string) error {
	err = ctx.JSON(http.StatusNotFound, getErrorResponse(
		base.NewGinfraErrorChildA("Not found.", err, base.LM_TARGET, target)))
	return err
}

func checkAndPostTerminalOk(ctx echo.Context, err error) error {
	if err != nil {
		panic(fmt.Sprintf("BUG: Escaped Error.  %v", err))
	}
	err = ctx.String(http.StatusOK, "OK")
	return err
}
