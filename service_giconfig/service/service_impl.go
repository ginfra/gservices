/*
Package service
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Service implementation.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package service

import (
	"bytes"
	"github.com/labstack/echo/v4"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common/service/shared"
	models "gitlab.com/ginfra/gservices/service_giconfig/api/service_giconfig/models"
	"gitlab.com/ginfra/gservices/service_giconfig/local"
	"gitlab.com/ginfra/gservices/service_giconfig/providers"
	"io"
	"net/http"
	"os"
	"path/filepath"

	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################

	"encoding/base64"
	"encoding/json"
	"gitlab.com/ginfra/ginfra/common"
	gconfig "gitlab.com/ginfra/ginfra/common/config"
	"strings"

	"fmt"
	"go.uber.org/zap"
)

type Servicegiconfig struct {
	Providers      *providers.ProvidersLoaded
	ServiceContext *local.GContext

	// # ADD ADDITIONAL SERVICE DATA START HERE
	// >B###############################################################################################################

	// >B###############################################################################################################
	// # ADD ADDITIONAL SERVICE DATA END HERE
}

func NewServicegiconfig(gcontext *local.GContext, providers *providers.ProvidersLoaded) (*Servicegiconfig, error) {
	return &Servicegiconfig{
		Providers:      providers,
		ServiceContext: gcontext,
	}, nil
}

func ServiceSetup(service *Servicegiconfig) error {

	// # ADD ADDITIONAL SETUP CODE START HERE.  All configuration is complete and the providers are ready.
	// >C###############################################################################################################

	// Make sure store/tmp exists.
	err := os.MkdirAll(filepath.Join(service.ServiceContext.ServiceHome, "store", "tmp"), os.ModePerm)

	// >C###############################################################################################################
	// # ADD ADDITIONAL SETUP CODE END HERE

	return err
}

var specifics string

func getServiceSpecifics(gis *Servicegiconfig) string {
	if specifics == "" {
		specifics = shared.LoadSpecifics(gis.ServiceContext.ServiceHome)
	}
	return specifics
}

// # STUB IMPLEMENTATIONS START HERE
// >E###############################################################################################################

func successfulQueryResponse(ctx echo.Context, path string, data map[string]interface{}) error {
	var cqr models.ConfigQueryResponse
	cqr.Qpath = &path
	d, _ := json.Marshal(data)
	b := base64.StdEncoding.EncodeToString(d) // I need a pointer to it.
	cqr.Block = &b
	return ctx.JSON(http.StatusOK, &cqr)
}

func processError(ctx echo.Context, err error, path string) error {
	local.Logger.Error("Error response.", zap.String(base.LM_CAUSE, err.Error()))
	if cerr, ok := err.(*base.GinfraError); ok {
		if (cerr.GetFlags() & base.ERRFLAG_NOT_AUTHORIZED) > 0 {
			return postErrorUnauthorizedResponse(ctx, path, cerr)
		} else if (cerr.GetFlags() & base.ERRFLAG_NOT_FOUND) > 0 {
			return postErrorNotFoundResponse(ctx, cerr, path)
		} else if (cerr.GetFlags() & base.ERRFLAG_SUBSYSTEM_SERIOUS) > 0 {
			return postErrorResponse(ctx, http.StatusInternalServerError, err)
		} else {
			return postErrorResponse(ctx, http.StatusBadRequest, cerr)
		}
	} else {
		return postErrorResponse(ctx, http.StatusInternalServerError, err)
	}
}

const GET_KIND_TYPE_SERVICE = "service"

// GiConfigQueryConfig. (GET /giconfig/query)
func (sc *Servicegiconfig) GiConfigQueryConfig(ctx echo.Context, params models.GiConfigQueryConfigParams) error {
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	c := sc.Providers.ConfigStore.Cp.FetchConfig(params.Qpath)

	var err error
	if c.Err == nil {
		err = successfulQueryResponse(ctx, params.Qpath, c.Config)
	} else {
		err = processError(ctx, c.Err, fmt.Sprintf("QUERY: %s", params.Qpath))
	}
	return err
}

func giConfigSetConfigHandleItem(sc *Servicegiconfig, ctx echo.Context, item *models.ConfigSetItem) error {

	var (
		datam map[string]interface{}
		err   error
	)

	if datam, err = common.Base64ToJsonMap(*item.Block); err != nil {
		return postErrorResponse(ctx, http.StatusBadRequest, err)
	}
	if err = sc.Providers.ConfigStore.Cp.PostConfig(gconfig.NewConfig(datam, nil),
		strings.Split(*item.Path, ".")...); err != nil {
		err = processError(ctx, err, fmt.Sprintf("QUERY: %s", *item.Path))
	}

	return err
}

// GiConfigSetConfig (POST /giconfig/set)
func (sc *Servicegiconfig) GiConfigSetConfig(ctx echo.Context, params models.GiConfigSetConfigParams) error {
	var (
		req models.ConfigServiceSetRequest
	)
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	err := ctx.Bind(&req)
	if err != nil {
		return postErrorResponse(ctx, http.StatusInternalServerError, err)

	} else {

		// Ignoring failmode for now.
		if len(*req.Items) < 1 {
			return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraError("Nothing in the request."))

		} else {
			for _, item := range *req.Items {
				err = giConfigSetConfigHandleItem(sc, ctx, &item)
				if err != nil {
					return err
				}
			}
		}
	}
	return checkAndPostTerminalOk(ctx, err)
}

// Register a service.
// (POST /giconfig/register)
func (sc *Servicegiconfig) GiConfigRegisterService(ctx echo.Context, params models.GiConfigRegisterServiceParams) error {
	var (
		req   models.ConfigServiceRegistrationRequest
		nauth string
	)
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	err := ctx.Bind(&req)
	if err != nil {
		return postErrorResponse(ctx, http.StatusInternalServerError, err)
	}

	var deployment string
	if req.Deployment == nil {
		deployment = gconfig.CR_DEPLOYMENT_DEFAULT
	} else {
		deployment = *req.Deployment
	}

	// - VALIDATE --------------------------------------------------------------------------------------------

	its, err := gconfig.GetInstanceTypeFromString(*req.Itype)
	if err != nil {
		return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraErrorA("Bat instance type.",
			base.LM_TYPE, its))

	}

	if req.Name == nil || len(*req.Name) < 0 {
		return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraError("Request missing service name."))
	}
	if req.Configprovider == nil || len(*req.Configprovider) < 0 {
		return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraError("Request missing configuration provider."))
	}
	if req.Port == nil || *req.Port < 23 || *req.Port > 65535 {
		return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraErrorA("Request has bad port value.",
			base.LM_SERVICE_PORT, *req.Port))
	}
	port := int(*req.Port)
	if req.Image == nil {
		ni := ""
		req.Image = &ni
	}

	// Service class does not have to be set.
	sclass := ""
	if req.Serviceclass != nil {
		sclass = *req.Serviceclass
	}

	// - MAKE IT  -----------------------------------------------------------------------------------------

	spec, err := common.Base64ToJsonMap(*req.Specifics)
	if err != nil {
		return postErrorResponse(ctx, http.StatusInternalServerError, err)
	}

	if nauth, err = sc.Providers.ConfigStore.Cp.RegisterService(its, *req.Name, deployment, *req.Auth, *req.Image,
		*req.Configprovider, sclass, port, spec); err == nil {

		// At this point if there are no errors, we have succeeded.
		var resp models.ConfigServiceRegistrationResponse
		resp.Confauth = &nauth
		resp.Name = req.Name
		err = ctx.JSON(http.StatusOK, resp)

	} else {
		err = processError(ctx, err, fmt.Sprintf("REGISTER: %s / %s", *req.Name, deployment))
	}

	return err
}

// Register a server instance.
// (POST /giconfig/instance)
func (sc *Servicegiconfig) GiConfigRegisterInstance(ctx echo.Context, params models.GiConfigRegisterInstanceParams) error {
	var (
		req  models.ConfigServiceInstanceRequest
		auth string
	)
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	err := ctx.Bind(&req)
	if err != nil {
		return postErrorResponse(ctx, http.StatusInternalServerError, err)
	}

	var deployment string
	if req.Deployment == nil {
		deployment = gconfig.CR_DEPLOYMENT_DEFAULT
	} else {
		deployment = *req.Deployment
	}

	// - VALIDATE --------------------------------------------------------------------------------------------

	its, err := gconfig.GetInstanceTypeFromString(*req.Itype)
	if err != nil {
		return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraErrorA("Bat instance type.",
			base.LM_TYPE, its))

	}

	if req.Name == nil || len(*req.Name) < 0 {
		return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraError("Request missing service name."))
	}
	if req.Sauth == nil || len(*req.Sauth) < 0 {
		return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraError("Request missing service friendly configuration auth."))
	}
	if req.Id == nil || len(*req.Id) < 0 {
		return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraError("Request missing service id."))
	}
	if req.Ip == nil || len(*req.Ip) < 0 {
		return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraError("Request missing service ip."))
	}
	if req.Fqdn == nil || len(*req.Fqdn) < 0 {
		return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraError("Request missing service FQDN."))
	}
	if req.Deployment == nil || len(*req.Deployment) < 0 {
		return postErrorResponse(ctx, http.StatusBadRequest, base.NewGinfraError("Request missing deployment name."))
	}

	// - CHECKS -------------------------------------------------------------------------------------------
	if auth, err = sc.Providers.ConfigStore.Cp.RegisterInstance(its, *req.Name, deployment, *req.Sauth, *req.Id, *req.Ip,
		*req.Fqdn); err == nil {

		// At this point if there are no errors, we have succeeded.
		var resp models.ConfigServiceRegistrationResponse
		resp.Confauth = &auth
		resp.Name = req.Name
		err = ctx.JSON(http.StatusOK, resp)

	} else {
		err = processError(ctx, err, fmt.Sprintf("REGISTER: %s / %s", *req.Name, deployment))
	}

	return err
}

func (sc *Servicegiconfig) GiConfigDiscoveryAdd(ctx echo.Context, params models.GiConfigDiscoveryAddParams) error {
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	err := sc.Providers.ConfigStore.Cp.DiscoveryAdd(params.Deployment, params.Sclass, params.Uri)
	if err != nil {
		err = processError(ctx, err, fmt.Sprintf("DISCOVERY ADD: %s / %s", params.Deployment, params.Sclass))

	} else {
		err = ctx.String(http.StatusOK, "OK")
	}
	return err
}

// Register or update a portal.
// (POST /giconfig/portal/{operation}/{deployment}/)
func (sc *Servicegiconfig) GiConfigPortal(ctx echo.Context, operation models.GiConfigPortalParamsOperation,
	deployment string, params models.GiConfigPortalParams) error {
	var (
		err error
		req models.ConfigPortalRequest
	)
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	err = ctx.Bind(&req)
	if err != nil {
		return postErrorResponse(ctx, http.StatusInternalServerError, err)
	}

	if gconfig.Operation(operation) == gconfig.OpPut {
		_, err = sc.Providers.ConfigStore.Cp.PortalAdd(deployment, *req.Sclass, *req.Portal)
		if err == nil {
			_ = ctx.String(http.StatusOK, "OK")
		} else {
			err = processError(ctx, err, fmt.Sprintf("PORTAL ADD: %s : %s", deployment, *req.Sclass))
		}

	} else if gconfig.Operation(operation) == gconfig.OpRemove {
		err = sc.Providers.ConfigStore.Cp.PortalRemove(deployment, *req.Sclass)
		if err == nil {
			_ = ctx.String(http.StatusOK, "OK")
		} else {
			err = processError(ctx, err, fmt.Sprintf("PORTAL REMOVE: %s : %s", deployment, *req.Sclass))
		}

	} else {
		err = processError(ctx, base.NewGinfraErrorA("Unknown portal operation.", base.LM_TOKEN, operation),
			fmt.Sprintf("PORTAL: %s / %s", deployment, *req.Sclass))
	}

	return err
}

// Deregister a service.
// (POST /giconfig/deregister/{name}/{deployment})
func (sc *Servicegiconfig) GiConfigDeregisterService(ctx echo.Context, deployment string, name string,
	params models.GiConfigDeregisterServiceParams) error {
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()

	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)
	err := sc.Providers.ConfigStore.Cp.DeregisterService(deployment, name)
	if err != nil {
		return processError(ctx, err, fmt.Sprintf("DEREGISTERSERVICE: %s / %s", name, deployment))
	}
	return ctx.String(http.StatusOK, "OK")
}

// Remove config.
// (GET /giconfig/remove)
func (sc *Servicegiconfig) GiRemoveConfig(ctx echo.Context, params models.GiRemoveConfigParams) error {
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	err := sc.Providers.ConfigStore.Cp.RemoveConfig(strings.Split(params.Qpath, ".")...)
	if err != nil {
		return processError(ctx, err, fmt.Sprintf("REMOVE: %s", params.Qpath))
	}
	return ctx.String(http.StatusOK, "OK")
}

// Discover a service.
// (GET /giconfig/discover/{deployment}/{serviceclass})
func (sc *Servicegiconfig) GiDiscoverService(ctx echo.Context, deployment string, serviceclass string,
	params models.GiDiscoverServiceParams) error {
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	url, err := sc.Providers.ConfigStore.Cp.DiscoverService(deployment, serviceclass)
	if err != nil {
		err = processError(ctx, err, fmt.Sprintf("DISCOVER SERVICE: %s / %s", deployment, serviceclass))

	} else {
		var resp models.DiscoverServiceResponse
		resp.Uri = &url
		err = ctx.JSON(http.StatusOK, resp)
		local.Logger.Info("Discovered service.", zap.String(base.LM_URL, url))
	}
	return err
}

// Discover a portal for a deployment.
// (GET /giconfig/discover/portal/{deployment}/{serviceclass})
func (sc *Servicegiconfig) GiDiscoverPortal(ctx echo.Context, deployment string, serviceclass string,
	params models.GiDiscoverPortalParams) error {
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	p, err := sc.Providers.ConfigStore.Cp.DiscoverPortal(deployment, serviceclass)
	if err != nil {
		p, err = sc.Providers.ConfigStore.Cp.DiscoverPortal(deployment, gconfig.CO_ANY_PORTAL)
	}
	if err != nil {
		return processError(ctx, err, fmt.Sprintf("DISCOVER PORTAL.  Portal not available: %s / %s",
			deployment, serviceclass))
	}

	var resp models.DiscoverPortalResponse
	resp.Uri = &p
	err = ctx.JSON(http.StatusOK, resp)
	local.Logger.Info("Discovered portal.", zap.String(base.LM_URL, p))

	return err
}

// Get the current service state.
// (GET /giconfig/state/get/{deployment}/{name}/)
func (sc *Servicegiconfig) GiConfigGetState(ctx echo.Context, deployment string, name string,
	params models.GiConfigGetStateParams) error {
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	i, err := sc.Providers.ConfigStore.Cp.GetState(deployment, name)
	if err != nil {
		return processError(ctx, err, fmt.Sprintf("INIT STATE.  Service not available: %s / %s",
			deployment, name))
	}

	b := common.JsonMapToBase64(i)
	var r models.ConfigGetStateResponse
	r.Block = &b

	return ctx.JSON(http.StatusOK, r)
}

// Get the current initializaton state.
// (GET /giconfig/state/init/{deployment}/{name}/{id}/)
func (sc *Servicegiconfig) GiConfigGetInit(ctx echo.Context, deployment string, name string, id string,
	params models.GiConfigGetInitParams) error {
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&sc.ServiceContext.AdminConfigToken)

	i, err := sc.Providers.ConfigStore.Cp.GetInitState(deployment, name, id)
	if err != nil {
		return processError(ctx, err, fmt.Sprintf("INIT STATE.  Service not available: %s / %s / %s",
			deployment, name, id))
	}

	var resp models.ConfigGetInitResponse
	ii := int(i)
	resp.State = &ii
	err = ctx.JSON(http.StatusOK, resp)

	return err
}

// Post State.
// (POST /giconfig/state/set/{deployment}/{name})
func (sc *Servicegiconfig) GiConfigPostState(ctx echo.Context, deployment string, name string, id string,
	params models.GiConfigPostStateParams) error {
	var (
		err error
		req models.ConfigServiceSetStateRequest
		s   map[string]interface{}
	)
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	err = ctx.Bind(&req)
	if err != nil {
		return postErrorResponse(ctx, http.StatusInternalServerError, err)
	}

	s, err = common.Base64ToJsonMap(*req.Block)
	if err != nil {
		return postErrorResponse(ctx, http.StatusInternalServerError, err)
	}

	err = sc.Providers.ConfigStore.Cp.PostState(deployment, name, id, s)
	if err != nil {
		return processError(ctx, err, fmt.Sprintf("INIT STATE.  Service not available: %s / %s / %s",
			deployment, name, ctx.RealIP()))
	}

	var r models.ConfigSetStateResponse
	state := int(gconfig.InitDone)
	r.State = &state
	return ctx.JSON(http.StatusOK, r)
}

// Store a file.
// (POST /giconfig/store/put)
func (sc *Servicegiconfig) GiConfigStorePut(ctx echo.Context, params models.GiConfigStorePutParams) error {
	sc.Providers.ConfigStore.Lock.Lock()
	defer sc.Providers.ConfigStore.Lock.Unlock()
	sc.Providers.ConfigStore.Cp.SetConfigAccessAuth(&params.Confauth)

	rc := ctx.Request().Body
	defer func(rc io.ReadCloser) {
		_ = rc.Close()
	}(rc)

	data, err := io.ReadAll(rc)
	if err != nil {
		return postErrorResponse(ctx, http.StatusInternalServerError, err)
	}

	var dp, sv string
	if params.Deployment != nil {
		dp = *params.Deployment
	}
	if params.Service != nil {
		sv = *params.Service
	}

	err = sc.Providers.ConfigStore.Cp.StorePut(params.Name, dp, sv, data)
	if err != nil {
		local.Logger.Error("Failed to store file due to provider error.", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Failed to store file.")
	}

	local.Logger.Info("File placed in store", zap.String(base.LM_NAME, params.Name))
	return ctx.String(http.StatusOK, "OK")
}

// Get a file.
// (POST /giconfig/store/get)
func (sc *Servicegiconfig) GiConfigStoreGet(ctx echo.Context, params models.GiConfigStoreGetParams) error {

	var dp, sv string
	if params.Deployment != nil {
		dp = *params.Deployment
	}
	if params.Service != nil {
		sv = *params.Service
	}

	data, err := sc.Providers.ConfigStore.Cp.StoreGet(params.Name, dp, sv, params.Raw)
	if err != nil {
		local.Logger.Error("Failed to get file due to provider error.", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Failed to get file.")
	}

	// Return the file as a response
	local.Logger.Info("File get from store.", zap.String(base.LM_NAME, params.Name))
	return ctx.Stream(http.StatusOK, "application/octet-stream", bytes.NewReader(data))
}

// >E###############################################################################################################
// # STUB IMPLEMENTATIONS END HERE
