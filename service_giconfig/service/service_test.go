//go:build functional_test

package service

import (
	"context"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ginfra/ginfra/api/service_giconfig/client"
	"gitlab.com/ginfra/ginfra/api/service_giconfig/models"
	testutil "gitlab.com/ginfra/ginfra/common/test"
	"testing"
	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################
	"gitlab.com/ginfra/ginfra/common"
	"gitlab.com/ginfra/ginfra/common/config"
	"gitlab.com/ginfra/gservices/service_giconfig/local"
	"net/http"
	"os"
	"path/filepath"
	// >A###############################################################################################################
	// #  YOUR IMPORTS END
	"strconv"
)

func yourSetup(t *testing.T, context *TestServiceContext) error {

	// # YOUR SETUP STARTS HERE
	// >B###############################################################################################################
	// Clear previous configs.

	files, err := filepath.Glob(filepath.Join(context.testServiceHome, local.CSF_STORE_DIRECTORY,
		config.CONFIG_FILE_PREFIX+"*"))
	if err != nil {
		panic(err)
	}
	for _, f := range files {
		if err := os.Remove(f); err != nil {
			panic(err)
		}
	}

	// Make sure tmp dir exists
	tf := filepath.Join(context.testServiceHome, "test", "tmp")
	if _, err = os.Stat(tf); err != nil {
		if errors.Is(err, os.ErrNotExist) {
			_ = os.MkdirAll(tf, os.ModePerm)
		} else {
			panic(fmt.Sprintf("Weird file problem.  %v", err))
		}
	}
	return nil

	// >B###############################################################################################################
	// #  YOUR SETUP ENDS HERE
}

func yourTeardown(t *testing.T, context *TestServiceContext) error {

	// # YOUR TEARDOWN STARTS HERE
	// >C###############################################################################################################

	return nil

	// >C###############################################################################################################
	// #  YOUR TEARDOWN ENDS HERE
}

/*
	IMPORTANT: Add the following two lines to the start of any Test* function.
		teardown, tcontext := setupTest(t, yourSetup)
		defer teardown(t, tcontext, yourTeardown)
*/
// ^^###################################################################################################################
// # ADDITIONAL CODE GOES BELOW HERE

var (
	TEST_DEPLOYMENT_NAME       = "default"
	TEST_SERVICE_NAME          = "testservice1"
	TEST_SERVICE_ID            = TEST_SERVICE_NAME
	TEST_SERVICE_CLASS         = "serviceclass1"
	TEST_CONFIG_PROV           = "configprov"
	TEST_PORT            int32 = 8888
	TEST_IMAGE                 = "image:tag"
	GOAT                       = "goat"
	MOUNTAINGOAT               = "mountaingoat"
	TEST_TYPE                  = "local"
	TEST_IP                    = "127.0.0.1"
	TEST_FQDN                  = "localhost"
	TEST_SERVICEURI            = "http://localhost:8888/"

	TEST_SECOND_DEPLOYMENT = "second1"
	TEST_BAD_DEPLOYMENT    = "bad"

	TEST_STORE_NAME = "borkblob.txt"

	NewServiceAuthToken string

	sspc map[string]interface{}

	regbody   models.GiConfigRegisterServiceJSONRequestBody
	regparams models.GiConfigRegisterServiceParams
	iparams   models.GiConfigRegisterInstanceParams
	ibody     models.GiConfigRegisterInstanceJSONRequestBody
)

func init() {
	sspc = map[string]interface{}{GOAT: MOUNTAINGOAT}
}

func testRegisterService(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {

	// -- Default deployment --------------------------------------------------------------
	regparams.Confauth = tctx.adminConfigToken
	regbody.Auth = &tctx.adminConfigToken
	regbody.Deployment = &TEST_DEPLOYMENT_NAME
	regbody.Itype = &TEST_TYPE
	regbody.Name = &TEST_SERVICE_NAME
	regbody.Serviceclass = &TEST_SERVICE_CLASS
	regbody.Configprovider = &TEST_CONFIG_PROV
	regbody.Port = &TEST_PORT
	regbody.Image = &TEST_IMAGE

	sb64 := common.JsonMapToBase64(sspc)
	regbody.Specifics = &sb64
	r, err := lc.GiConfigRegisterServiceWithResponse(context.Background(), &regparams, regbody)
	if err != nil {
		t.Error(fmt.Errorf("Could not get register service.  %v", err))
		return
	}

	assert.Equal(t, 200, r.StatusCode(), "Http status code")
	if r.StatusCode() == 200 {
		assert.Equal(t, config.AUTH_TOKEN_LENGTH, len(*r.JSON200.Confauth), "Authtoken")
		NewServiceAuthToken = *r.JSON200.Confauth // We will use this in later tests
		assert.Equal(t, TEST_SERVICE_NAME, *r.JSON200.Name, "Service name")
	} else {
		fmt.Println(string(r.Body))
	}

	// -- Cannot reregister ------------------------------------------------------------
	r, err = lc.GiConfigRegisterServiceWithResponse(context.Background(), &regparams, regbody)
	assert.Equal(t, 400, r.StatusCode(), "Http status code")

	// -- Register another -------------------------------------------------------------
	regbody.Deployment = &TEST_SECOND_DEPLOYMENT
	r, err = lc.GiConfigRegisterServiceWithResponse(context.Background(), &regparams, regbody)
	assert.Equal(t, 200, r.StatusCode(), "Second deployment: Http status code")
	if r.StatusCode() == 200 {
		assert.Equal(t, config.AUTH_TOKEN_LENGTH, len(*r.JSON200.Confauth), "Second deployment: Authtoken")
		assert.Equal(t, TEST_SERVICE_NAME, *r.JSON200.Name, "Second deployment:Service name")
	}
}

func testRegisterInstance(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {

	// -- Default deployment --------------------------------------------------------------
	iparams.Confauth = tctx.adminConfigToken
	ibody.Name = &TEST_SERVICE_NAME
	ibody.Sauth = &NewServiceAuthToken
	ibody.Deployment = &TEST_DEPLOYMENT_NAME
	ibody.Itype = &TEST_TYPE
	ibody.Id = &TEST_SERVICE_ID
	ibody.Ip = &TEST_IP
	ibody.Fqdn = &TEST_FQDN
	r, err := lc.GiConfigRegisterInstanceWithResponse(context.Background(), &iparams, ibody)
	if err != nil {
		t.Error(fmt.Errorf("Could not get register service instance.  %v", err))
		return
	}

	assert.Equal(t, 200, r.StatusCode(), "Http status code")
	if r.StatusCode() == 200 {
		assert.Equal(t, config.AUTH_TOKEN_LENGTH, len(*r.JSON200.Confauth), "Authtoken")
		assert.Equal(t, TEST_SERVICE_NAME, *r.JSON200.Name, "Service name")
	}

	// -- Cannot reregister ------------------------------------------------------------
	ibody.Deployment = &TEST_BAD_DEPLOYMENT
	r, err = lc.GiConfigRegisterInstanceWithResponse(context.Background(), &iparams, ibody)
	assert.Equal(t, 400, r.StatusCode(), "Http status code")
}

func testSetConfig(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {
	var (
		params models.GiConfigSetConfigParams
		body   models.GiConfigSetConfigJSONRequestBody
	)
	params.Confauth = tctx.adminConfigToken

	// Testing the tools too.
	var items []models.ConfigSetItem
	configPath1 := "service.cannonball.hippo"
	configJsonMap1, _ := common.StringToJsonmap(`{
		"car": {
			"color": "red",
			"age": 5
		}
	}`)
	configBase641 := common.JsonMapToBase64(configJsonMap1)
	item1 := models.ConfigSetItem{
		Path:  &configPath1,
		Block: &configBase641,
	}
	items = append(items, item1)

	configPath2 := "service.cannonball.lion"
	configJsonMap2, _ := common.StringToJsonmap(`{
		"truck": {
			"color": "white",
			"age": 10
		}
	}`)
	configBase642 := common.JsonMapToBase64(configJsonMap2)
	item2 := models.ConfigSetItem{
		Path:  &configPath2,
		Block: &configBase642,
	}
	items = append(items, item2)

	body.Items = &items
	r, err := lc.GiConfigSetConfigWithResponse(context.Background(), &params, body)
	if err != nil {
		t.Error(fmt.Errorf("Could not set config.  %v", err))
		return
	}
	assert.Equal(t, 200, r.StatusCode(), "Http status code set config")

}

func testQueryConfig(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {

	var (
		params models.GiConfigQueryConfigParams
	)
	params.Confauth = tctx.adminConfigToken
	params.Qpath = "service"
	r, err := lc.GiConfigQueryConfigWithResponse(context.Background(), &params)
	if err != nil {
		t.Error(fmt.Errorf("Could not query config.  %v", err))
		return
	}

	assert.Equal(t, http.StatusOK, r.StatusCode(), "Http status query 1")
	if r.StatusCode() == http.StatusOK {
		c := config.NewConfig(common.Base64ToJsonMap(*r.JSON200.Block))
		vv, ve := c.GetConfigPath("cannonball.hippo.car").GetValue("color").GetValueString()
		assert.Equal(t, nil, ve, "Http status query 1-1")
		assert.Equal(t, "red", vv, "Http status query 1-1")
		vv, ve = c.GetConfigPath("cannonball.lion.truck").GetValue("color").GetValueString()
		assert.Equal(t, "white", vv, "Http status query 1-2")
		vv, ve = c.GetConfigPath("cannonball.goose.truck").GetValue("color").GetValueString()
		assert.NotEqualValues(t, nil, ve, "Http status query 4")
	}
	params.Confauth = "AAAAAAAAAA"
	r, err = lc.GiConfigQueryConfigWithResponse(context.Background(), &params)
	assert.Equal(t, 401, r.StatusCode(), "Http status query bad auth") // We don't auth local
}
func testAddDiscovery(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {
	var (
		params models.GiConfigDiscoveryAddParams
	)
	params.Confauth = tctx.adminConfigToken
	params.Deployment = TEST_DEPLOYMENT_NAME
	params.Sclass = TEST_SERVICE_CLASS
	params.Uri = TEST_SERVICEURI

	r, err := lc.GiConfigDiscoveryAddWithResponse(context.Background(), &params)
	if err != nil {
		t.Error(fmt.Errorf("Could not get discover service.  %v", err))
		return
	}
	assert.Equal(t, http.StatusOK, r.StatusCode(), "Could not add discovery.")
}

func testServiceDiscovery(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {

	var (
		params models.GiDiscoverServiceParams
	)

	// -- Default deployment --------------------------------------------------------------
	params.Confauth = tctx.adminConfigToken
	r, err := lc.GiDiscoverServiceWithResponse(context.Background(), TEST_DEPLOYMENT_NAME, TEST_SERVICE_CLASS, &params)
	if err != nil {
		t.Error(fmt.Errorf("Could not get discover service.  %v", err))
		return
	}
	assert.Equal(t, http.StatusOK, r.StatusCode(), "Http status code")
	if r.StatusCode() == http.StatusOK {
		assert.Equal(t, TEST_SERVICEURI, *r.JSON200.Uri, "Discovery uri")
	}

	r, err = lc.GiDiscoverServiceWithResponse(context.Background(), "sdasdfgas", TEST_SERVICE_CLASS, &params)
	if r == nil {
		t.Error(fmt.Errorf("Config service is toast.  %v", err))
	} else {
		assert.NotEqual(t, http.StatusOK, r.StatusCode(), "Http status code not ok.")
	}
}

func testPortalDiscovery(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {

	var (
		params models.GiDiscoverPortalParams
	)

	// -- Default deployment --------------------------------------------------------------
	params.Confauth = tctx.adminConfigToken
	r, err := lc.GiDiscoverPortalWithResponse(context.Background(), TEST_DEPLOYMENT_NAME, TEST_SERVICE_CLASS, &params)
	if err != nil {
		t.Error(fmt.Errorf("Could not get portal service.  %v", err))
		return
	}
	assert.Equal(t, http.StatusOK, r.StatusCode(), "Http status code")
	if r.StatusCode() == http.StatusOK {
		assert.Equal(t, TEST_SERVICEURI, *r.JSON200.Uri, "Discovery portal")
	}
}

func testStateInit(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {
	var (
		params models.GiConfigGetInitParams
	)
	params.Confauth = NewServiceAuthToken

	i, err := lc.GiConfigGetInitWithResponse(context.Background(), TEST_DEPLOYMENT_NAME, TEST_SERVICE_NAME,
		TEST_SERVICE_ID, &params)
	if err != nil {
		t.Error(fmt.Errorf("Could not get init..  %v", err))
		return
	}
	if i.StatusCode() != http.StatusOK {
		t.Error(fmt.Errorf("Got error code.  %v", i.StatusCode()))
		return
	}

	assert.Equal(t, config.InitNew, *i.JSON200.State, "Should be InitNew")

	i, err = lc.GiConfigGetInitWithResponse(context.Background(), TEST_DEPLOYMENT_NAME, TEST_SERVICE_NAME,
		"asdasdasfa", &params)
	assert.Equal(t, config.InitPending, *i.JSON200.State, "Should be InitPending")
}

func testStateInitDone(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {
	var (
		params models.GiConfigGetInitParams
	)
	params.Confauth = NewServiceAuthToken

	i, err := lc.GiConfigGetInitWithResponse(context.Background(), TEST_DEPLOYMENT_NAME, TEST_SERVICE_NAME,
		TEST_SERVICE_ID, &params)
	if err != nil {
		t.Error(fmt.Errorf("Could not get init for done.  %v", err))
		return
	}
	assert.Equal(t, config.InitDone, *i.JSON200.State, "Should be InitDone")
}

func testStateGetBad(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {
	var (
		params models.GiConfigGetStateParams
	)
	params.Confauth = NewServiceAuthToken

	_, err := lc.GiConfigGetStateWithResponse(context.Background(), TEST_DEPLOYMENT_NAME, TEST_SERVICE_NAME, &params)
	if err != nil {
		t.Error(fmt.Errorf("Could not get for bad.  %v", err))
		return
	}
}

func testStatePost(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {
	var (
		params models.GiConfigPostStateParams
		body   models.GiConfigPostStateJSONRequestBody
	)
	params.Confauth = NewServiceAuthToken
	s := common.JsonMapToBase64(map[string]interface{}{
		"monkey": "horse",
		"goat":   "zebra"})
	body.Block = &s

	// deployment string, name string, id string, params *GiConfigPostStateParams, body GiConfigPostStateJSONRequestBody, reqEditors ...RequestEditorFn)
	r, err := lc.GiConfigPostStateWithResponse(context.Background(), TEST_DEPLOYMENT_NAME, TEST_SERVICE_NAME,
		"testservice1", &params, body)
	if err != nil {
		t.Error(fmt.Errorf("Could not set for done.  %v", err))
		return
	}
	assert.Equal(t, http.StatusOK, r.StatusCode(), "Trying to set state.")
}

func testStateGet(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {
	var (
		params models.GiConfigGetStateParams
	)
	params.Confauth = NewServiceAuthToken

	r, err := lc.GiConfigGetStateWithResponse(context.Background(), TEST_DEPLOYMENT_NAME, TEST_SERVICE_NAME, &params)
	if err != nil {
		t.Error(fmt.Errorf("Could not get state.  %v", err))
		return
	}
	assert.Equal(t, http.StatusOK, r.StatusCode(), "Trying to get state.")
	if r.StatusCode() == http.StatusOK {
		m, err := common.Base64ToJsonMap(*r.JSON200.Block)
		if err != nil {
			t.Error(fmt.Errorf("Could not decodestate.  %v", err))
			return
		}
		assert.Equal(t, "horse", m["monkey"], "Check state 1.")
		assert.Equal(t, "zebra", m["goat"], "Check state 2.")
	}

}

func testState(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {

	t.Run("State Init", func(t *testing.T) {
		testStateInit(t, tctx, lc)
	})
	t.Run("State Get Bad", func(t *testing.T) {
		testStateGetBad(t, tctx, lc)
	})
	t.Run("State Post", func(t *testing.T) {
		testStatePost(t, tctx, lc)
	})
	t.Run("State Get", func(t *testing.T) {
		testStateGet(t, tctx, lc)
	})
	t.Run("State Init Done", func(t *testing.T) {
		testStateInitDone(t, tctx, lc)
	})
}

func testDeregisterService(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {

	var (
		params  models.GiConfigDeregisterServiceParams
		qparams models.GiConfigQueryConfigParams
	)

	// -- Default deployment --------------------------------------------------------------
	params.Confauth = tctx.adminConfigToken
	r, err := lc.GiConfigDeregisterServiceWithResponse(context.Background(), "default", TEST_SERVICE_NAME,
		&params)
	if err != nil {
		t.Error(fmt.Errorf("Could not get deregister service.  %v", err))
		return
	}
	assert.Equal(t, 200, r.StatusCode(), "Http status code")

	qparams.Confauth = tctx.adminConfigToken
	qparams.Qpath = "services." + TEST_SERVICE_NAME + "default"
	q, err := lc.GiConfigQueryConfigWithResponse(context.Background(), &qparams)
	assert.Equal(t, 404, q.StatusCode(), "Http status code")

}

func testReRegisterService(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {

	// -- Default deployment --------------------------------------------------------------
	regbody.Deployment = &TEST_DEPLOYMENT_NAME
	r, err := lc.GiConfigRegisterServiceWithResponse(context.Background(), &regparams, regbody)
	if err != nil {
		t.Error(fmt.Errorf("Could not get register service.  %v", err))
		return
	}

	assert.Equal(t, 200, r.StatusCode(), "Http status code")
	if r.StatusCode() == 200 {
		assert.Equal(t, config.AUTH_TOKEN_LENGTH, len(*r.JSON200.Confauth), "Authtoken")
		NewServiceAuthToken = *r.JSON200.Confauth // We will use this in later tests
		assert.Equal(t, TEST_SERVICE_NAME, *r.JSON200.Name, "Service name")
	}
}

func testReRegisterInstance(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {

	ibody.Deployment = &TEST_DEPLOYMENT_NAME
	r, err := lc.GiConfigRegisterInstanceWithResponse(context.Background(), &iparams, ibody)
	if err != nil {
		t.Error(fmt.Errorf("Could not get reregister service instance.  %v", err))
		return
	}

	assert.Equal(t, 200, r.StatusCode(), "Http status code")
	if r.StatusCode() == 200 {
		assert.Equal(t, config.AUTH_TOKEN_LENGTH, len(*r.JSON200.Confauth), "Authtoken")
		assert.Equal(t, TEST_SERVICE_NAME, *r.JSON200.Name, "Service class name")
	}

	// -- Cannot reregister ------------------------------------------------------------
	ibody.Deployment = &TEST_BAD_DEPLOYMENT
	r, err = lc.GiConfigRegisterInstanceWithResponse(context.Background(), &iparams, ibody)
	assert.Equal(t, 400, r.StatusCode(), "Http status code")
}

func testStorePut(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {

	params := models.GiConfigStorePutParams{
		Name:     TEST_STORE_NAME,
		Confauth: tctx.adminConfigToken,
	}

	f, err := os.Open(filepath.Join(tctx.gctx.ServiceHome, "test/forstore.txt"))
	if err != nil {
		t.Error("Test is broken.  Could not open the test file.")
		return
	}
	defer func(f *os.File) {
		_ = f.Close()
	}(f)

	r, err := lc.GiConfigStorePutWithBodyWithResponse(context.Background(), &params, "application/octet-stream",
		f)
	assert.Equal(t, http.StatusOK, r.StatusCode(), "Http status code")
}

func testStoreGet(t *testing.T, tctx *TestServiceContext, lc *client.ClientWithResponses) {

	params := models.GiConfigStoreGetParams{
		Name:     TEST_STORE_NAME,
		Confauth: tctx.adminConfigToken,
		Raw:      true,
	}

	f, err := os.Create(filepath.Join(tctx.gctx.ServiceHome, "test/fromstore.txt"))
	if err != nil {
		t.Error(fmt.Sprintf("Test is broken.  Could not open target file.  %v", err))
		return
	}
	defer func(f *os.File) {
		if f != nil {
			_ = f.Close()
		}
	}(f)

	r, err := lc.GiConfigStoreGetWithResponse(context.Background(), &params)
	assert.Equal(t, http.StatusOK, r.StatusCode(), "Http status code")
	if r.StatusCode() != http.StatusOK {
		return
	}

	// Read the response body and write it to the target file.
	_, err = f.Write(r.Body)
	if err != nil {
		t.Error("Test is broken.  Could not write to target file.")
		return
	}

	err = f.Close()
	if err != nil {
		t.Error("Test is broken.  Could not close target file.")
		return
	}

	b, err := os.ReadFile(filepath.Join(tctx.gctx.ServiceHome, "test/fromstore.txt"))
	if err != nil {
		t.Error("Test is broken.  Could not open target file for verification.")
		return
	}

	sp, _ := testutil.NewScanPack(string(b), nil)
	assert.Less(t, 1, sp.Seek("tore testing"))
}

func TestServiceGIConfig(t *testing.T) {

	teardown, tcontext := setupTest(t, yourSetup)
	defer teardown(t, tcontext, yourTeardown)

	hostname, _ := os.Hostname()
	lc, err := client.NewClientWithResponses("http://" + hostname + ":" + strconv.Itoa(config.RESERVED_CONFIG_SERVICE_PORT))
	if err != nil {
		t.Error(fmt.Errorf("Could not get client.  %v", err))
		return
	}

	t.Run("Register service", func(t *testing.T) {
		testRegisterService(t, tcontext, lc)
	})
	t.Run("Register instance", func(t *testing.T) {
		testRegisterInstance(t, tcontext, lc)
	})
	t.Run("Set config", func(t *testing.T) {
		testSetConfig(t, tcontext, lc)
	})
	t.Run("Query config", func(t *testing.T) {
		testQueryConfig(t, tcontext, lc)
	})
	t.Run("Add Discovery", func(t *testing.T) {
		testAddDiscovery(t, tcontext, lc)
	})
	t.Run("Service Discovery", func(t *testing.T) {
		testServiceDiscovery(t, tcontext, lc)
	})
	t.Run("Test Portal Discovery", func(t *testing.T) {
		testPortalDiscovery(t, tcontext, lc)
	})
	testState(t, tcontext, lc)
	t.Run("Deregister Service", func(t *testing.T) {
		testDeregisterService(t, tcontext, lc)
	})
	t.Run("Reregster Service", func(t *testing.T) {
		testReRegisterService(t, tcontext, lc)
	})
	t.Run("Reresgister Instance", func(t *testing.T) {
		testReRegisterInstance(t, tcontext, lc)
	})
	t.Run("Store Put", func(t *testing.T) {
		testStorePut(t, tcontext, lc)
	})
	t.Run("Store Get", func(t *testing.T) {
		testStoreGet(t, tcontext, lc)
	})
}
