package providers

import (
	"gitlab.com/ginfra/ginfra/common/config"
	"gitlab.com/ginfra/gservices/service_giconfig/local"
	"path/filepath"
	"sync"
	"time"
)

type ConfigStoreProvider struct {
	Cp   config.ConfigProvider
	Lock *sync.Mutex
}

type ConfigStorePutPackage struct {
	path string
	data map[string]interface{}
}

func GetConfigStoreProvider(gcontext *local.GContext) (ConfigStoreProvider, error) {
	var (
		provider ConfigStoreProvider
		err      error
		lock     sync.Mutex
	)
	provider.Lock = &lock

	if provider.Cp, err = config.GetConfigProviderFileSavable(filepath.Join(gcontext.ServiceHome,
		local.CSF_STORE_DIRECTORY), 10, true); err == nil {
		c := config.NewConfig(nil, nil)
		c.PutValue(config.CR_TIME, time.Now())
		if err = provider.Cp.PostConfig(c, config.CR_LIVE); err != nil {
			return provider, err
		}

		ca := provider.Cp.FetchConfig(config.CR_AUTH)
		if ca.Err != nil || ca.GetValue(config.CR_AUTH).GetValueStringNoerr() != gcontext.AdminConfigToken {
			c := config.NewConfig(nil, nil)
			c.PutValue(config.CR_AUTH, gcontext.AdminConfigToken)
			err = provider.Cp.PostConfig(c, "")
		}
		err = c.Err
	}

	return provider, err
}
