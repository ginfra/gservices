/*
Package providers
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Service providers.  ConfigStore is there ny default.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package providers

import (
	"gitlab.com/ginfra/gservices/service_giconfig/local"
	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################
	// >A###############################################################################################################
	// #  YOUR IMPORTS END
)

type ProvidersLoaded struct {

	// # ADD POINTERS (without * because they are interfaces) TO YOUR PROVIDERS START HERE
	// >B###############################################################################################################

	ConfigStore ConfigStoreProvider

	// >B###############################################################################################################
	// # ADD POINTERS (without * because they are interfaces) TO YOUR PROVIDERS END HERE

}

func ProvidersLoad(gcontext *local.GContext) (*ProvidersLoaded, error) {
	var (
		l   ProvidersLoaded
		err error
	)

	// # ADD INSTANTIATING AND REMEMBERING YOUR PROVIDERS START HERE
	// >C###############################################################################################################

	l.ConfigStore, err = GetConfigStoreProvider(gcontext)

	// >C###############################################################################################################
	// # ADD INSTANTIATING AND REMEMBERING YOUR PROVIDERS END HERE

	return &l, err
}
