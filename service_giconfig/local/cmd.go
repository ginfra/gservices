/*
Package local
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Local command processor.  This is the bridge between the command line arguments and a configured context.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package local

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common"
	"gitlab.com/ginfra/ginfra/common/config"
	"path"
)

var (
	rpath     string
	clearData bool
)

var RootCmd = &cobra.Command{
	Use:   "",
	Short: "Service start",
	Long: `Configuration provider service.  It accepts a single argument that is the root path to the service.  
In that path it expects to find a configuration file named "service_config.json".`,
	Run: func(cmd *cobra.Command, args []string) {
		rpath = args[0]
	},
	Args: cobra.MinimumNArgs(1),
}

func init() {
	// This is so we don't have to edit the conf file when testing in the ide.
	RootCmd.PersistentFlags().BoolVar(&clearData, config.CL_CLEAR,
		false, "DANGER: tell the service to clear its data stores.")
}

func (gctx *GContext) GetCmdContext() error {
	var (
		err error
		ok  bool
		cur string
	)
	gctx.ServiceHome = rpath

	// I know, I'm treating it as an exception, but the code gets really nasty if I ddo it all with ifs.
	defer func() {
		r := recover()
		if r != nil {
			gctx.Serr = base.NewGinfraErrorA("Failed to configure service due to bad configuration in "+base.ConfigFileName,
				base.LM_CONFIG_NAME, cur, base.LM_CAUSE, fmt.Sprintf("%v", r))
		}
	}()

	var cfg map[string]interface{}
	err = common.ReadJsonFileMap(path.Join(rpath, base.ConfigFileName), "service_config.json configuration file", &cfg)
	if err == nil {

		if _, ok = cfg[base.ConfigClear]; ok || clearData {
			gctx.Clear = true
		}

		cur = base.ConfigAccessToken
		gctx.AdminConfigToken = cfg[base.ConfigAccessToken].(string)
		if gctx.InstanceType, err = config.GetInstanceTypeFromString(cfg[base.ConfigType].(string)); err != nil {
			err = base.NewGinfraErrorA("Bad instance type.",
				base.LM_TYPE, cfg[base.ConfigType].(string))
		}

		if gctx.InstanceType == config.ITLocal && gctx.AdminConfigToken == "" {
			gctx.AdminConfigToken = config.CONFIG_NIL_AUTH
		}
	}

	return err
}

// ^^###################################################################################################################
// # ADDITIONAL CODE GOES BELOW HERE
