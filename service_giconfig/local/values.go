/*
Package local
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Local values.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package local

import (
	"go.uber.org/zap"
	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################
	// >A###############################################################################################################
	// #  YOUR IMPORTS END
)

// #####################################################################################################################
// # STATIC

const ServiceName = "giconfig"

// It is always thia port for this config service.
const ServicePort = 8881

const CSF_STORE_DIRECTORY = "store"
const CSF_STORE_TMP_DIRECTORY = "store/tmp"

// #####################################################################################################################
// # VALUES

var (
	Logger *zap.Logger
)

// ^^###################################################################################################################
// # ADDITIONAL CODE GOES BELOW HERE
