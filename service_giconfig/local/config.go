/*
Package local
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Local config.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package local

import (
	"gitlab.com/ginfra/ginfra/common/config"
)

type GContext struct {

	// Flavors
	InstanceType config.InstanceType

	// Values
	ServiceHome      string
	ConfigStoreUri   string
	AdminConfigToken string
	Specifics        string
	Hostname         string

	// Orders
	Clear bool // Clear data stores and start fresh.

	// Private
	it string // For command line
	dp string // Thrown away

	// Pending system error.
	Serr error
}

var GlobalGContext *GContext

// GetContext returns a configured instance of GContext.  It should be treated as read-only.
func GetContext() (*GContext, error) {
	var err error

	// A race condition is possible here, but I don't think it matters much since these should be read only.
	if GlobalGContext == nil {
		var c GContext
		err = c.GetCmdContext()
		GlobalGContext = &c
	}

	return GlobalGContext, err
}

// ^^###################################################################################################################
// # ADDITIONAL CODE GOES BELOW HERE
