/*
Package local
Copyright (c) 2023 Erich Gatejen
[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt if unaltered.  You are welcome to alter and apply any
license you wish to this file.

Values.
*/
package local

import (
	"go.uber.org/zap"
	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################
	// >A###############################################################################################################
	// #  YOUR IMPORTS END
)

// #####################################################################################################################
// # STATIC

const ServiceName = "server_uptrace"
const TestAuth = "XXXXXXXXXXXX"

// #####################################################################################################################
// # VALUES

var (
	Logger *zap.Logger
)

// ^^###################################################################################################################
// # ADDITIONAL CODE GOES BELOW HERE
