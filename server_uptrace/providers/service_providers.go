/*
Package providers
Copyright (c) 2023 Erich Gatejen
[LICENSE]

The user provider.  It is just a simple read only file for the purposes of demonstration.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package providers

import (
	"ginfra.com/ginfra/gservices/server_uptrace/local"
	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################
	// >A###############################################################################################################
	// #  YOUR IMPORTS END
)

type ProvidersLoaded struct {

	// # ADD POINTERS (without * because they are interfaces) TO YOUR PROVIDERS START HERE
	// >B###############################################################################################################

	// >B###############################################################################################################
	// # ADD POINTERS (without * because they are interfaces) TO YOUR PROVIDERS END HERE

}

func ProvidersLoad(gcontext *local.GContext) (*ProvidersLoaded, error) {
	var (
		l   ProvidersLoaded
		err error
	)

	// # ADD INSTANTIATING AND REMEMBERING YOUR PROVIDERS START HERE
	// >C###############################################################################################################

	// >C###############################################################################################################
	// # ADD INSTANTIATING AND REMEMBERING YOUR PROVIDERS END HERE

	return &l, err
}
