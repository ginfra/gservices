#!/usr/bin/env bash

##########################################################################
# Place custom entry tasks here to be done before starting the server.
# This file will not be updated, so any additions or changes are safe.

########  CLICK HOUSE

# Probably will move this into service setup.
cp -f /users.xml /etc/clickhouse-server
cp -f /config.xml /etc/clickhouse-server
service clickhouse-server start

sleep 1
clickhouse-client -q "CREATE DATABASE uptrace"

######## POSTGRES

echo "Starting postgres..." >> /service/log/start.log
pg_ctlcluster 16 main start >> /service/log/start.log

su -c 'psql -f setup_db.sql' postgres
su -c 'psql -d uptrace -f setup_db_uptrace.sql' postgres



