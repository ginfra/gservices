#!/usr/bin/env bash

##########################################################################
# Place custom entry provision tasks performed during docker build here
# This file will not be updated, so any additions or changes are safe.

echo "Custom provisioning unique to this service."

## Click House

apt-get update
apt-get install -y apt-transport-https ca-certificates wget curl gnupg gnupg2 sudo
curl -fsSL 'https://packages.clickhouse.com/rpm/lts/repodata/repomd.xml.key' |  gpg --dearmor -o /usr/share/keyrings/clickhouse-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/clickhouse-keyring.gpg] https://packages.clickhouse.com/deb stable main" | tee \
    /etc/apt/sources.list.d/clickhouse.list
apt-get update

export DEBIAN_FRONTEND=noninteractive
apt-get install --allow-unauthenticated --yes --no-install-recommends  clickhouse-server clickhouse-client

## Postgres

addgroup --system --gid 500 postgres; \
    adduser --system --ingroup postgres --uid 500 --home /home/postgres --shell /bin/bash postgres; \
    chown -R postgres:postgres /home/postgres

wget -q -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | gpg --dearmor -o /etc/apt/trusted.gpg.d/postgresql.gpg
sh -c 'echo "deb https://apt.postgresql.org/pub/repos/apt bookworm-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
apt update -y
apt install -y postgresql-16 sudo

cp -f /etc/postgresql/16/main/postgresql.conf /etc/postgresql/16/main/postgresql.conf.OLD
cp -f /etc/postgresql/16/main/pg_hba.conf  /etc/postgresql/16/main/pg_hba.conf.OLD
cp -f /postgresql.conf /etc/postgresql/16/main/postgresql.conf.NEW
cp -f /postgresql.conf /etc/postgresql/16/main/postgresql.conf

pg_ctlcluster 16 main start

cp -f /pg_hba.conf /etc/postgresql/16/main/pg_hba.conf.NEW
cp -f /pg_hba.conf /etc/postgresql/16/main/pg_hba.conf

pg_ctlcluster 16 main stop

rm -f install.sh

## Uptrace

wget https://github.com/uptrace/uptrace/releases/download/v1.7.6/uptrace_1.7.6_amd64.deb
dpkg -i uptrace_1.7.6_amd64.deb
rm -f uptrace_1.7.6_amd64.deb
mv -f uptrace.yml /etc/uptrace
