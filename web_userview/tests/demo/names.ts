describe('Check names', function () {
  before((browser) => {
    browser.init()
  })

  it('get bob', function () {
    browser
        //.url(process.env.VUE_DEV_SERVER_URL)
        .waitForElementVisible('#app', 5000)
        .setValue("input.inputusername", "bob")
        .click(".doit")
        .waitForElementVisible(".results")
        .assert.textContains(".results", "Username: bob")

  })

  after((browser) => browser.end())
})
