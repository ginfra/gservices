// General API stuff

import {Giconfig} from "@/api/giconfig";
import {Giuser} from "@/api/giuser";

export interface ApiClientInfo {
    initializing:   boolean;

    configurl:      string;
    accesstoken:    string;
    deployment:     string;

    configClient?:  Giconfig;

    userProviderUri?:    string;

    serverFault?:        string;

    // Add actual AUTH later.
}

export let LiveApiClientInfo : ApiClientInfo

export async function InitApiClient() {

    return new Promise<void>((resolve, reject) => {
        LiveApiClientInfo = {
            initializing: true,
            configurl: import.meta.env.VITE_GINFRA_CONFIG_URI,
            accesstoken: import.meta.env.VITE_GINFRA_ACCESS_TOKEN,
            deployment: import.meta.env.VITE_GINFRA_DEPLOYMENT,
            serverFault: ""
        }
        if ((LiveApiClientInfo.configurl == undefined) || (LiveApiClientInfo.configurl == "") ||
            (LiveApiClientInfo.accesstoken == undefined) || (LiveApiClientInfo.accesstoken == "") ||
            (LiveApiClientInfo.deployment == undefined) || (LiveApiClientInfo.deployment == "")) {
            throw Error("Configuration provider access properties not complete.")
        }
        LiveApiClientInfo.configClient = new Giconfig({
            BASE: LiveApiClientInfo.configurl,
        });
        console.log("Start configuration.")
        doConfig(LiveApiClientInfo.configClient)
            .then(() => {
                console.log("Configuration complete.")
            })
            .catch((err) => {
                console.log("Configuration failed.")
                LiveApiClientInfo.serverFault = err.toString()
            })
            .finally(() => {
                resolve()
            })
    })

}

async function doConfig(configClient: Giconfig) : Promise<void> {
    return new Promise<void>((resolve, reject) => {

        configClient.default.giGetSpecifics()
            .then((specifics) => {

                getUserProvider(configClient)
                    .then(() => {
                        resolve()
                    })
                    .catch((err) => {
                        reject("Could not get user provider.  " + err.toString())
                    })

            })
            .catch((err) => {
                reject("Configuration service unavailable.  " + err.toString())
            })
    })
}

// Not chaining these as may use this separately.
async function getUserProvider(configClient: Giconfig) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
        configClient.default.giDiscoverPortal(LiveApiClientInfo.deployment, "giuser", LiveApiClientInfo.accesstoken)
            .then((uri) => {
                let thing = uri.uri!
                LiveApiClientInfo.userProviderUri = thing.replace(/\/+$/, '')
                resolve()
            })
            .catch((err) => {
                reject(err)
            })
    })
}


// =================================================================================================
// == Userinfo

export type userstateinfo = {
    error?: string
}

import {type userinfov2 as giuserinfo} from "@/api/giuser/models/userinfov2"
export function getUserInfo(info: giuserinfo, state: userstateinfo) {
    const userClient = new Giuser({
        BASE: LiveApiClientInfo.userProviderUri,
    });
    userClient.default.getGiuserV2User(info.username!)
        .then(resp => {
            info.username = resp.username!
            info.firstname = resp.firstname!
            info.lastname = resp.lastname!
            state.error = ""
        })
        .catch(err => {
            info.username = ""
            info.firstname = ""
            info.lastname = ""
            state.error = err.toString()
        })
}