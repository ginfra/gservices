/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { giusername } from './giusername';

export type userinfov2 = {
    coins?: number;
    firstname?: string;
    lastname?: string;
    username?: giusername;
};

