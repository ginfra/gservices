/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { base64 } from './base64';

export type ConfigServiceRegistrationRequest = {
    /**
     * Service control authorization token.
     */
    auth?: string;
    /**
     * URI to configuration provider.  This is not authoritive.
     */
    configprovider?: string;
    /**
     * Deployment name.  If blank it will be the default deployment.
     */
    deployment?: string;
    image?: string;
    /**
     * Instance host type
     */
    itype?: string;
    /**
     * System unique name
     */
    name?: string;
    /**
     * Exposed service port
     */
    port?: number;
    serviceclass?: string;
    specifics?: base64;
};

