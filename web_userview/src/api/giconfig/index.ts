/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export { Giconfig } from './Giconfig';

export { ApiError } from './core/ApiError';
export { BaseHttpRequest } from './core/BaseHttpRequest';
export { CancelablePromise, CancelError } from './core/CancelablePromise';
export { OpenAPI } from './core/OpenAPI';
export type { OpenAPIConfig } from './core/OpenAPI';

export type { authtoken } from './models/authtoken';
export type { base64 } from './models/base64';
export type { ConfigGetInitResponse } from './models/ConfigGetInitResponse';
export type { ConfigGetStateResponse } from './models/ConfigGetStateResponse';
export type { ConfigPath } from './models/ConfigPath';
export type { ConfigPortalRequest } from './models/ConfigPortalRequest';
export type { ConfigQueryResponse } from './models/ConfigQueryResponse';
export type { ConfigServiceInstanceRequest } from './models/ConfigServiceInstanceRequest';
export type { ConfigServiceRegistrationRequest } from './models/ConfigServiceRegistrationRequest';
export type { ConfigServiceRegistrationResponse } from './models/ConfigServiceRegistrationResponse';
export { ConfigServiceSetRequest } from './models/ConfigServiceSetRequest';
export type { ConfigServiceSetStateRequest } from './models/ConfigServiceSetStateRequest';
export type { ConfigSetItem } from './models/ConfigSetItem';
export type { ConfigSetStateResponse } from './models/ConfigSetStateResponse';
export type { ControlActionResponse } from './models/ControlActionResponse';
export type { ControlDatasourceInitRequest } from './models/ControlDatasourceInitRequest';
export type { ControlDatasourceInitResponse } from './models/ControlDatasourceInitResponse';
export type { DiscoverPortalResponse } from './models/DiscoverPortalResponse';
export type { DiscoverServiceResponse } from './models/DiscoverServiceResponse';
export type { ErrorResponse } from './models/ErrorResponse';
export { GetKindType } from './models/GetKindType';
export type { giusername } from './models/giusername';
export type { namevalue } from './models/namevalue';
export type { Specifics } from './models/Specifics';

export { DefaultService } from './services/DefaultService';
