/*
Package local
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Values for this service.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package local

import (
	"go.uber.org/zap"
	// # YOUR IMPORTS START HERE
	// >A###############################################################################################################
	// >A###############################################################################################################
	// #  YOUR IMPORTS END
)

// #####################################################################################################################
// # STATIC

const ServiceName = "server_mongodb"
const TestAuth = "XXXXXXXXXXXX"

// #####################################################################################################################
// # VALUES

var (
	Logger *zap.Logger
)

// ^^###################################################################################################################
// # ADDITIONAL CODE GOES BELOW HERE
