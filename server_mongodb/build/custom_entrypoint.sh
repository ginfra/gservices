#!/usr/bin/env bash

###########################################
# Place custom entry tasks here to be done before starting the server.
# This file will not be updated, so any additions or changes are safe.

echo "Starting Mongodb in the background."  > /service/log/start.log
mongod --bind_ip_all 2>&1 > /service/log/start.log &
