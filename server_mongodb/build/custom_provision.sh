#!/usr/bin/env bash

##########################################################################
# Place custom entry provision tasks performed during docker build here
# This file will not be updated, so any additions or changes are safe.


echo "Custom provisioning unique to this service."

groupadd --gid 500 --system mongodb; \
    useradd --uid 500 --system --gid mongodb --home-dir /mongodb mongodb; \
    mkdir -p /mongodb; \
    chown -R mongodb:mongodb /mongodb; \
    mkdir /configdb; \
    chown -R mongodb:mongodb /configdb

apt-get update; \
    apt-get install -y --no-install-recommends ca-certificates gnupg curl; \
    rm -rf /var/lib/apt/lists/

curl -fsSL https://www.mongodb.org/static/pgp/server-7.0.asc | \
    gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg --dearmor

echo "deb [ signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] http://repo.mongodb.org/apt/debian bookworm/mongodb-org/7.0 main" | \
    tee /etc/apt/sources.list.d/mongodb-org-7.0.list

apt-get update
apt-get install -y mongodb-org

mkdir /data
chown mongodb:mongodb /data
mkdir /data/db
chown mongodb:mongodb /data/db

