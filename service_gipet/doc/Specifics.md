# Specifics

This is the additional information available to Ginfra on how to run the service.

## Configuration 

Specifics are contained in the file [specifics.json][../specifics.json].  The file must
be present at runtime but may be an empty json (simple a "{}".)

## Sections

There are none for this service.
