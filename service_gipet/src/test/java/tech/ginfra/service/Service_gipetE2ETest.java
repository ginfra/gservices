package tech.ginfra.service;

import client.micronaut.api.DefaultApi;
import client.micronaut.model.Pettype;
import io.micronaut.context.env.PropertySource;
import io.micronaut.runtime.Micronaut;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import tech.ginfra.Ginfra;

import java.util.HashMap;
import java.util.Map;

@MicronautTest
class Service_gipetE2ETest {

    private static Micronaut mn;

    private static EmbeddedServer server;
    @BeforeAll
    public static void setupServer() throws Exception {
        Ginfra.LoadGinfra(System.getenv("TEST_HOME"));

        String[] args = {};
        mn = Micronaut.build(args);
        mn.classes(Application.class);

        Map<String, Object> props = new HashMap<>();
        props.put("micronaut.server.port", 8080);

        // Properties managed by ginfra.
        var ps = PropertySource.of("MySource", props);
        mn.propertySources(ps);
        mn.start();
        // I should die when it falls out of scope.
    }

    @Test
    void testCliente3E(DefaultApi api) {
        var pet = api.gipetPetGet(Pettype.DOG);
        Assertions.assertEquals("Klee Kai", pet.getBreed());
        pet = api.gipetPetGet(Pettype.CAT);
        Assertions.assertNotEquals("", pet.getBreed());
    }

}
