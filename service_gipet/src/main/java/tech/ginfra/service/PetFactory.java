package tech.ginfra.service;

import micronaut.model.Petinfo;
import micronaut.model.Pettype;
import tech.ginfra.service.exception.InternalErrorException;

import java.util.Random;

public class PetFactory {

    static String[] cats = {"American Shorthair", "Balinese", "Bengal", "British Longhair", "British Shorthair",
            "Burmese", "Highlander", "Himalayan", "Korat", "Manx", "Ocicat", "Ragamuffin", "Russian Blue", "Savannah",
            "Siamese", "Sokoke", "Sphynx", "Turkish Angora", "Ukrainian Levkoy"};

    static String[] llamas = { "Wooly Llama", "Classic Llama", "Suri Llama", "Ccaras Llama", "Chakus Llama"};

    public static Petinfo getPet(Pettype type) {
        var pet = new Petinfo();
        pet.pettype(type);

        switch(type) {

            case CAT:
                pet.breed(cats[new Random().nextInt(cats.length)]);
                break;

            case DOG:
                // Everybody gets a Klee Kai!
                pet.breed("Klee Kai");
                break;

            case LLAMA:
                pet.breed(llamas[new Random().nextInt(llamas.length)]);
                break;

            default:
                throw new InternalErrorException("BUG: unknown pet type made it to the controller.  This should never have happened.", "");
        }

        return pet;
    }
}
