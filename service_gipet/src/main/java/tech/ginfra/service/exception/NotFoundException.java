package tech.ginfra.service.exception;

public class NotFoundException extends RuntimeException {
    public String message;

    public NotFoundException() {
        message = "Not found";
    }

    public NotFoundException(String msg) {
        message = msg;
    }
}
