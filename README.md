# Gservices

This repo contains tools and services for use with the [Ginfra application](https://gitlab.com/ginfra/ginfra/).

It provides an end-to-end demonstration that can be found [here](integration/demo/README.md).

There is also a more complicated end-to-end demonstration with more features and services
that can be found [here](integration/scenerios/newdemo/README.md).

IMPORTANT: It works best if you clone this repo into the same directory as you cloned [Ginfra](https://gitlab.com/ginfra/ginfra/).
Additionally, they should be both at the same branch (unless you are in active development, of course).

## Setup

### Nodejs

Most of the setup will be accomplished by installing the Ginfra application.  However, some of the services need
[nodejs](https://nodejs.org/) and npm installed to build.  On debian/ubuntu this can be accomplished with the following 
command:
```apt-get update ; apt-get install -y npm nodejs```

See [these instructions](https://nodejs.org/en/download/package-manager) for other platforms.

Note that the demos mentioned above do need this.

### Java

Some of the services need [java](https://openjdk.org/install/) to run (but not to build).  [See our note](https://gitlab.com/ginfra/ginfra/-/blob/main/lang/java/README.md) about Ginfra java and follow any instructions.

The second and third demos mentioned above do need this.

## The services

Three services are provided--enough to demonstrate an end-to-end environment.  These are put into a
separate repo because they are not actually needed for Ginfra to work.  Ginfra does directly support
the service_giconfig service, but it isn't a requirement for all features.

- [service_giconfig](service_giconfig/) : This 
    is a configuration service.  Generally speaking you will need something of the sort.  Ginfra itself 
    has a local file based configuration provider.  Or if you don't want to use this service, you can create 
    your own.  An OpenApi spec file for it is in the file [ginfra/ginterfaces/service_giconfig.json](https://gitlab.com/ginfra/ginfra/-/blob/main/ginterfaces/service_giconfig.yaml).  
    If you implement this then the ginfra app and any of these services can work with it.
- [service_giuser](service_giuser/) : This is a simple user 
    information service.  It is an example of how you can let the ginfra app create and manage the service's 
    framework, leaving you to only implement the business logic and interfaces.
- [service_gipet](service_gipet/) : This is a simple pet
  information service.  It has the same architecture as service_giuser.
- [web_userview](web_userview/) : A Vue.js/Vite/Typescript web 
    application that can interact with the user information service.
- [web_userapp](web_userview/) : A Vue.js/Vite/Typescript web
  application that can interact with the user information service.  It is an improved version of web_userview.
- [server_otelcol](server_otelcol/) : An [Open Telemetry](https://opentelemetry.io/) [collector](https://opentelemetry.io/docs/collector/).
    This server/service takes observability data from other services, so it can be forwarded to  
    some sort of observability data store and applications.
- [server_uptrace](server_uptrace/) : An [Uptrace](https://uptrace.dev/) deployment that provides store and application
    features of observability.
- [service_selenium](service_selenium/) : A [selenium](https://www.selenium.dev/) hub service that can run snippets of 
    java to drive selenium.  It is primarily used for testing and driving traffic.

There are additional services, but they have no integrated demo at this time.

## Additional demos

### Observability end-to-end

This demo is for host mode only, which requires a docker host to run.  It will run an end-to-end 
demo using the services mentioned above.   See the [otel demo](integration/workbench/otel/host/) for more information.

### Builds

The taskfile.yaml has various builds for the services.  Generally, the specific demo you want to run will tell you which 
build to execute.  All rely on the task application, which is part of the Ginfra app installation, so you should already 
have it.  Most rely on the setup steps mentioned above.

If you see the following error...
```
no unqualified-search registries are defined in "/etc/containers/registries.conf"
```
... run the following commands and try again:
```
sed -i -e 's/example.com/docker.io/g' /etc/containers/registries.conf
sed -i -e 's/# unqualified-search/unqualified-search/g' /etc/containers/registries.conf
```