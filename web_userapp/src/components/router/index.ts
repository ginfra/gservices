import { createRouter, createWebHistory } from 'vue-router'
import AboutView from "@/views/AboutView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'about',
      component: AboutView
    },
    {
      path: '/user',
      name: 'user',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../../views/UserView.vue')
    },
    {
      path: '/pet',
      name: 'pet',
      component: () => import('../../views/PetView.vue')
    },
    {
      path: '/error',
      name: 'error',
      component: () => import('../../views/ErrorView.vue')
    }
  ]
})

export default router
