/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export { Giuser } from './Giuser';

export { ApiError } from './core/ApiError';
export { BaseHttpRequest } from './core/BaseHttpRequest';
export { CancelablePromise, CancelError } from './core/CancelablePromise';
export { OpenAPI } from './core/OpenAPI';
export type { OpenAPIConfig } from './core/OpenAPI';

export type { authtoken } from './models/authtoken';
export type { base64 } from './models/base64';
export type { coinreceipt } from './models/coinreceipt';
export type { ErrorResponse } from './models/ErrorResponse';
export type { giusername } from './models/giusername';
export type { namevalue } from './models/namevalue';
export type { Specifics } from './models/Specifics';
export type { userinfov2 } from './models/userinfov2';

export { DefaultService } from './services/DefaultService';
