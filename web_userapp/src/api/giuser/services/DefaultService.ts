/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { coinreceipt } from '../models/coinreceipt';
import type { giusername } from '../models/giusername';
import type { userinfov2 } from '../models/userinfov2';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class DefaultService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @param username
     * @returns userinfov2 User found
     * @throws ApiError
     */
    public getGiuserV2User(
        username: giusername,
    ): CancelablePromise<userinfov2> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/giuser/v2/user/{username}',
            path: {
                'username': username,
            },
            errors: {
                404: `Not found`,
            },
        });
    }

    /**
     * @param username
     * @param numbercoins
     * @returns coinreceipt Coins used
     * @throws ApiError
     */
    public getGiuserV2UserCoinsUse(
        username: giusername,
        numbercoins: number,
    ): CancelablePromise<coinreceipt> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/giuser/v2/user/{username}/coins/use/{numbercoins}',
            path: {
                'username': username,
                'numbercoins': numbercoins,
            },
            errors: {
                406: `Not enough coins.`,
            },
        });
    }

}
