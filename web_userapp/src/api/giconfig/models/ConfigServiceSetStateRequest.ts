/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { base64 } from './base64';

export type ConfigServiceSetStateRequest = {
    block?: base64;
};

