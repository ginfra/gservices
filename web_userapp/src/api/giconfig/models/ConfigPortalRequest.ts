/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ConfigPortalRequest = {
    /**
     * Typically an address or DN for the portal.
     */
    portal?: string;
    /**
     * Service class.
     */
    sclass?: string;
};

