/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ControlDatasourceInitResponse = {
    password: string;
    port: number;
    uri: string;
    username: string;
};

