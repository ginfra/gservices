// General API stuff

import {Giconfig} from "@/api/giconfig";
import {Giuser} from "@/api/giuser";
import {Gipet} from "@/api/gipet";

export interface ApiClientInfo {
    initializing:   boolean;

    configurl:      string;
    accesstoken:    string;
    deployment:     string;

    configClient?:  Giconfig;
    giuserClient?:  Giuser;
    gipetClient?:   Gipet;

    serverFault?:        string;

    // Add actual AUTH later.
}

export let LiveApiClientInfo : ApiClientInfo

export async function InitApiClient() {

    return new Promise<void>((resolve, reject) => {

        LiveApiClientInfo = {
            initializing: true,
            configurl: import.meta.env.VITE_GINFRA_CONFIG_URI,
            accesstoken: import.meta.env.VITE_GINFRA_ACCESS_TOKEN,
            deployment: import.meta.env.VITE_GINFRA_DEPLOYMENT,
            serverFault: ""
        }
        if ((LiveApiClientInfo.configurl == undefined) || (LiveApiClientInfo.configurl == "") ||
            (LiveApiClientInfo.accesstoken == undefined) || (LiveApiClientInfo.accesstoken == "") ||
            (LiveApiClientInfo.deployment == undefined) || (LiveApiClientInfo.deployment == "")) {
            LiveApiClientInfo.serverFault = "Configuration provider access properties not complete."
            resolve()
        }
        LiveApiClientInfo.configClient = new Giconfig({
            BASE: LiveApiClientInfo.configurl,
        });
        console.log("Start configuration.")
        doConfig(LiveApiClientInfo.configClient)
            .then((results) => {
                LiveApiClientInfo.giuserClient = results[1]
                LiveApiClientInfo.gipetClient = results[2]
                console.log("Configuration complete.")
            })
            .catch((err) => {
                console.log("Configuration failed.")
                LiveApiClientInfo.serverFault = err.toString()
                //useRouter().push("/error")
                //reject("Configuration failed.")
            })
            .finally(() => {
                resolve()
            })
    })
}

async function doConfig(configClient: Giconfig) : Promise<[Giconfig, Giuser, Gipet]> {
    return Promise.all([getPingConfig(configClient), getUserProvider(configClient, "giuser"),
        getPetProvider(configClient, "gipet")])
}

async function getPingConfig(configClient: Giconfig) : Promise<Giconfig> {
    return new Promise<Giconfig>((resolve, reject) => {
        configClient.default.giGetSpecifics()
            .then((uri) => {
                resolve(configClient)
            })
            .catch((err) => {
                reject("Configuration service unavailable.  " + err.toString())
            })
    })
}

// Tried to make a template but these templates are even worse that C++.  I'll try another day.
async function getUserProvider(configClient: Giconfig, serviceClass: string) : Promise<Giuser> {
    return new Promise<Giuser>((resolve, reject) => {
        configClient.default.giDiscoverPortal(LiveApiClientInfo.deployment, serviceClass, LiveApiClientInfo.accesstoken)
            .then((uri) => {
                let thing = uri.uri!
                let newClient = new Giuser({
                    BASE: thing.replace(/\/+$/, ''),
                });
                resolve(newClient)
            })
            .catch((err) => {
                reject("Could not get provider portal for " + serviceClass + " : " + err.toString())
            })
    })
}

async function getPetProvider(configClient: Giconfig, serviceClass: string) : Promise<Gipet> {
    return new Promise<Gipet>((resolve, reject) => {
        configClient.default.giDiscoverPortal(LiveApiClientInfo.deployment, serviceClass, LiveApiClientInfo.accesstoken)
            .then((uri) => {
                let thing = uri.uri!
                let newClient = new Gipet({
                    BASE: thing.replace(/\/+$/, ''),
                });
                resolve(newClient)
            })
            .catch((err) => {
                reject("Could not get provider portal for " + serviceClass + " : " + err.toString())
            })
    })
}

// =================================================================================================
// == Userinfo

export type userstateinfo = {
    error?: string
}

import {type userinfov2 as giuserinfo} from "@/api/giuser/models/userinfov2"
export function getUserInfo(info: giuserinfo, state: userstateinfo) {
    LiveApiClientInfo.giuserClient?.default.getGiuserV2User(info.username!)
        .then(resp => {
            info.username = resp.username!
            info.firstname = resp.firstname!
            info.lastname = resp.lastname!
            info.coins = resp.coins!
            state.error = ""
        })
        .catch(err => {
            info.username = ""
            info.firstname = ""
            info.lastname = ""
            info.coins = 0
            state.error = err.toString()
        })
}

// =================================================================================================
// == Pet

export type petstateinfo = {
    error?: string
}

import {type petinfo as gipetinfo} from "@/api/gipet/models/petinfo"
export function getPet(info: gipetinfo, state: petstateinfo) {
    LiveApiClientInfo.gipetClient?.default.getGipetPet(info.pettype!)
        .then(resp => {
            info.pettype= resp.pettype!
            info.breed = resp.breed!
            state.error = ""
        })
        .catch(err => {
            info.breed = ""
            state.error = err.toString()
        })
}