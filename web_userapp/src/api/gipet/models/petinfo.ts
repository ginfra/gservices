/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { pettype } from './pettype';

export type petinfo = {
    breed?: string;
    pettype?: pettype;
};

