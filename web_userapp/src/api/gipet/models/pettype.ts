/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum pettype {
    DOG = 'dog',
    CAT = 'cat',
    LLAMA = 'llama',
}
