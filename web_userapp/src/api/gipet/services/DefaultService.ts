/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { petinfo } from '../models/petinfo';
import type { pettype } from '../models/pettype';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class DefaultService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @param pettype
     * @returns petinfo Pet found.
     * @throws ApiError
     */
    public getGipetPet(
        pettype: pettype,
    ): CancelablePromise<petinfo> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/gipet/pet',
            headers: {
                'pettype': pettype,
            },
            errors: {
                404: `Not found`,
                500: `Internal Server Error`,
            },
        });
    }

}
